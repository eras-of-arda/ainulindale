package eoa.ainulindale.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.discovery.ASMDataTable.ASMData;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.EventBus;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.reflection.CommonReflectionHelpers;

public class AinulindaleAPI {
    public static final EventBus LOTR_EVENT_BUS = new EventBus();
    
    private static List<Submod> submods = new ArrayList<Submod>();
    
    public static void findSubmods(FMLPreInitializationEvent event) {
        List<String> submodClasses = event.getAsmData().getAll(SubmodMarker.class.getName()).stream().map(ASMData::getClassName).collect(Collectors.toList());
        
        for(Object o : Loader.instance().getModObjectList().values()) {
            if(submodClasses.contains(o.getClass().getName())) {
                submods.add((Submod) o);
                submodClasses.remove(o.getClass().getName());
            }
        }
        
        for(String submodClass : submodClasses) {
            submods.add(CommonReflectionHelpers.Submods.createSubmod(submodClass));
        }
        
        for(Submod submod : submods) submod.loadSubmod();
        for(Submod submod : submods) submod.enableSubmod();
        
        Ainulindale.log.info("Loaded " + submods.size() + " submods.");
    }
    

}
