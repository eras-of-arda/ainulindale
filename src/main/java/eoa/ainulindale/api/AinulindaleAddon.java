package eoa.ainulindale.api;

import eoa.ainulindale.api.registries.*;
import eoa.ainulindale.common.Ainulindale;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.LOTRShields;
import lotr.common.LOTRTitle;
import lotr.common.fac.LOTRFaction;
import lotr.common.fac.LOTRFactionRelations;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.map.LOTRMountains;
import lotr.common.world.map.LOTRRoads;
import lotr.common.world.map.LOTRWaypoint;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Abstract class for an addon.
 * <p>
 * Your addon should extend this class and call setupAddon to be recognized by Ainulindalë.
 */
public abstract class AinulindaleAddon { //extend this for an addon

    public static String API_VERSION = Ainulindale.API_VERSION;

    /**
     * Initialize and register your addon. Call this from your preInit method.
     *
     * @param addonID the ID of your addon. It is recommended, but not required, to make this your modid
     * @param keepVanillaBiomes whether to keep vanilla biomes (other than key essential biomes which are always kept)
     * @param keepVanillaFactions whether to keep vanilla factions
     * @param mapInfo the map for the addon, as an Ainulindalë MapInfo object
     * @param factionExclusions an array of LOTRFaction objects to keep if keepVanillaFactions is false, or remove if keepVanillaFactions is true
     * @param biomeExclusions an array of LOTRBiome objects to keep if keepVanillaBiomes is false, or remove if keepVanillaBiomes is true
     */
    public final void setupAddonPreInit(String addonID, boolean keepVanillaBiomes, boolean keepVanillaFactions, boolean keepVanillaWaypoints, boolean keepVanillaFactionRegions, boolean keepVanillaHills, boolean keepVanillaShields, MapRegistry.MapInfo mapInfo, LOTRFaction[] factionExclusions, LOTRBiome[] biomeExclusions, LOTRWaypoint[] waypointExclusions, LOTRDimension.DimensionRegion[] factionRegionExclusions, LOTRMountains[] hillExclusions, LOTRShields[] shieldExclusions)
    {
        if(!API_VERSION.equals(Ainulindale.API_VERSION)) //if it has been set to a diff version
        {
            Ainulindale.log.warn("Mismatched Ainulindalë API version detected! "+API_VERSION+" != "+Ainulindale.API_VERSION);
        }

        if(addonID == "lotr")
        {
            throw new IllegalArgumentException("Addon ID cannot be 'lotr': that ID is reserved internally");
        }
        if(AddonRegistry.addonExistsWithName(addonID))
        {
            throw new IllegalArgumentException("Addon ID '"+addonID+"' already in use");
        }
        AddonRegistry.registerAddon(addonID);
        MapRegistry.registerMap(mapInfo);
        AddonRegistry.setAddonForMapID(mapInfo.getID(), addonID, mapInfo);

        Ainulindale.log.info("Setting up vanilla content for addon "+addonID);

        if(keepVanillaBiomes)
        {
           Ainulindale.log.info("keeping vanilla "+BiomeRegistry.getBiomeList("lotr").keySet().size());
           for(int id : BiomeRegistry.getBiomeList("lotr").keySet())
           {
               Ainulindale.log.info("1 Checking vanilla biome "+BiomeRegistry.getBiomeEntry("lotr", id)+" ("+id+") for "+addonID);
               boolean excluded = false;
               for(LOTRBiome biome2 : biomeExclusions) {
                   if(biome2.biomeID == id)
                   {
                       excluded = true;
                   }
               }
               if(!excluded) {
                   LOTRBiome biome = BiomeRegistry.getBiomeEntry("lotr", id);
                   System.out.println("Putting " + biome.biomeName);
                   int color = biome.color;
                   BiomeRegistry.setBiomeEntry(addonID, id, biome, color);
               }
           }
        }
        else
        {
            Ainulindale.log.info("keeping vanilla "+BiomeRegistry.getBiomeList("lotr").keySet().size());
            for(int id : BiomeRegistry.getBiomeList("lotr").keySet())
            {
                Ainulindale.log.info("2 Checking vanilla biome "+BiomeRegistry.getBiomeEntry("lotr", id)+" ("+id+") for "+addonID);
                for(LOTRBiome biome2 : biomeExclusions) {
                    if(biome2.biomeID == id)
                    {
                        LOTRBiome biome = BiomeRegistry.getBiomeEntry("lotr", id);
                        System.out.println("Putting "+biome.biomeName);
                        int color = biome.color;
                        BiomeRegistry.setBiomeEntry(addonID, id, biome, color);
                        break;
                    }
                }
                if(id == 0 || id == 35 || id == 63 || id == 94 || id == 95 || id == 150 || id == 83) //override for river, ocean, lake, beaches, island
                {
                    LOTRBiome biome = BiomeRegistry.getBiomeEntry("lotr", id);
                    System.out.println("Putting "+biome.biomeName);
                    int color = biome.color;
                    BiomeRegistry.setBiomeEntry(addonID, id, biome, color);
                }
            }
        }

        if(keepVanillaFactions)
        {
            for(LOTRFaction fac : FactionRegistry._getVanillaFactions())
            {
                boolean excluded = false;
                for(LOTRFaction fac2 : factionExclusions)
                {
                    if(fac == fac2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    FactionRegistry.addFactionToList(addonID, fac);
                }
            }
        }
        else
        {
            for(LOTRFaction fac : FactionRegistry._getVanillaFactions())
            {
                for(LOTRFaction fac2 : factionExclusions)
                {
                    if(fac == fac2) {
                        FactionRegistry.addFactionToList(addonID, fac);
                    }
                }
            }
        }

        WaypointRegistry.createWaypointList(addonID);

        if(keepVanillaWaypoints)
        {
            for(LOTRWaypoint wp : WaypointRegistry.getWaypoints("lotr"))
            {
                boolean excluded = false;
                for(LOTRWaypoint wp2 : waypointExclusions)
                {
                    if(wp == wp2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    WaypointRegistry.registerWaypointWithoutAdding(addonID, wp);
                }
            }
        }
        else
        {
            for(LOTRWaypoint wp : WaypointRegistry.getWaypoints("lotr"))
            {
                for(LOTRWaypoint wp2 : waypointExclusions)
                {
                    if(wp == wp2) {
                        WaypointRegistry.registerWaypointWithoutAdding(addonID, wp);
                    }
                }
            }
        }

        if(keepVanillaFactionRegions)
        {
            for(LOTRDimension.DimensionRegion region : FactionRegistry.getFactionRegionsList("lotr"))
            {
                boolean excluded = false;
                for(LOTRDimension.DimensionRegion region2 : factionRegionExclusions)
                {
                    if(region == region2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    FactionRegistry.addFactionRegionAsObject(addonID, region.getRegionName(), region);
                }
            }
        }
        else
        {
            for(LOTRDimension.DimensionRegion region : FactionRegistry.getFactionRegionsList("lotr"))
            {
                for(LOTRDimension.DimensionRegion region2 : factionRegionExclusions)
                {
                    if(region == region2) {
                        FactionRegistry.addFactionRegionAsObject(addonID, region.getRegionName(), region);
                    }
                }
            }
        }


        if(keepVanillaHills)
        {
            for(LOTRMountains hill : LOTRMountains.values())
            {
                boolean excluded = false;
                for(LOTRMountains hill2 : hillExclusions)
                {
                    if(hill == hill2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    HillRegistry.addHill(addonID, hill);
                }
            }
        }
        else
        {
            for(LOTRMountains hill : LOTRMountains.values())
            {
                for(LOTRMountains hill2 : hillExclusions)
                {
                    if(hill == hill2) {
                        HillRegistry.addHill(addonID, hill);
                    }
                }
            }
        }


        if(keepVanillaShields)
        {
            for(LOTRShields shield : LOTRShields.values())
            {
                boolean excluded = false;
                for(LOTRShields shield2 : shieldExclusions)
                {
                    if(shield == shield2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    ShieldRegistry.registerShield(addonID, shield);
                }
            }
        }
        else
        {
            for(LOTRShields shield : LOTRShields.values())
            {
                for(LOTRShields shield2 : shieldExclusions)
                {
                    if(shield == shield2) {
                        ShieldRegistry.registerShield(addonID, shield);
                    }
                }
            }
        }


    }

    public final void setupAddonPostInit(String addonID, boolean keepVanillaAchievements, boolean keepVanillaAchievementCategories, boolean keepVanillaMapLabels, boolean keepVanillaTitles, boolean keepVanillaRoads, LOTRAchievement[] achievementExclusions, LOTRAchievement.Category[] categoryExclusions, LOTRMapLabels[] mapLabelExclusions, LOTRTitle[] titleExclusions, LOTRRoads[] roadExclusions)
    {

        if(keepVanillaTitles)
        {
            for(LOTRTitle title : TitleRegistry.getTitles("lotr"))
            {
                boolean excluded = false;
                for(LOTRTitle title2 : titleExclusions)
                {
                    if(title == title2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    TitleRegistry.registerTitle(addonID, title);
                }
            }
        }
        else
        {
            for(LOTRTitle title : TitleRegistry.getTitles("lotr"))
            {
                for(LOTRTitle title2 : titleExclusions)
                {
                    if(title == title2) {
                        TitleRegistry.registerTitle(addonID, title);
                    }
                }
            }
        }

        if(keepVanillaAchievements)
        {
            for(LOTRAchievement achievement : AchievementRegistry.getAchievementList("lotr"))
            {
                boolean excluded = false;
                for(LOTRAchievement achievement2 : achievementExclusions)
                {
                    if(achievement == achievement2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    AchievementRegistry.registerAchievement(addonID, achievement);
                }
            }
        }
        else
        {
            for(LOTRAchievement achievement : AchievementRegistry.getAchievementList("lotr"))
            {
                for(LOTRAchievement achievement2 : achievementExclusions)
                {
                    if(achievement == achievement2) {
                        AchievementRegistry.registerAchievement(addonID, achievement);
                    }
                }
            }
        }


        if(keepVanillaAchievementCategories)
        {
            for(LOTRAchievement.Category achievementCategory : AchievementRegistry.getCategoryList("lotr"))
            {
                boolean excluded = false;
                for(LOTRAchievement.Category achievementCategory2 : categoryExclusions)
                {
                    if(achievementCategory == achievementCategory2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    AchievementRegistry.registerCategory(addonID, achievementCategory);
                }
            }
        }
        else
        {
            for(LOTRAchievement.Category achievementCategory : AchievementRegistry.getCategoryList("lotr"))
            {
                for(LOTRAchievement.Category achievementCategory2 : categoryExclusions)
                {
                    if(achievementCategory == achievementCategory2) {
                        AchievementRegistry.registerCategory(addonID, achievementCategory);
                    }
                }
            }
        }


        if(keepVanillaMapLabels)
        {
            for(LOTRMapLabels label : MapLabelRegistry.getLabelList("lotr"))
            {
                boolean excluded = false;
                for(LOTRMapLabels label2 : mapLabelExclusions)
                {
                    if(label == label2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    MapLabelRegistry.registerMapLabel(addonID, label);
                }
            }
        }
        else
        {
            for(LOTRMapLabels label : MapLabelRegistry.getLabelList("lotr"))
            {
                for(LOTRMapLabels label2 : mapLabelExclusions)
                {
                    if(label == label2) {
                        MapLabelRegistry.registerMapLabel(addonID, label);
                    }
                }
            }
        }

        if(keepVanillaRoads)
        {
            //System.out.println("ABOUT TO CHECK ROADS for addon "+addonID);
            for(LOTRRoads road : RoadRegistry.getRoadsList("lotr"))
            {
                //System.out.println("CHECKING ROAD "+road.getDisplayName()+" for addon "+addonID);
                boolean excluded = false;
                for(LOTRRoads road2 : roadExclusions)
                {
                    if(road == road2) {
                        excluded = true;
                    }
                }
                if(!excluded) {
                    RoadRegistry.addRoad(addonID, road);
                }
            }
        }
        else
        {
            for(LOTRRoads road : RoadRegistry.getRoadsList("lotr"))
            {
                for(LOTRRoads road2 : roadExclusions)
                {
                    if(road == road2) {
                        RoadRegistry.addRoad(addonID, road);
                    }
                }
            }
        }

        for(LOTRFactionRelations.FactionPair pair : FactionRegistry._getFacRelationsListVanilla().keySet())
        {
            if(FactionRegistry.isFacInAddon(addonID, pair.getLeft()) && FactionRegistry.isFacInAddon(addonID, pair.getRight()))
            {
                FactionRegistry.setFacRelations(addonID, pair, FactionRegistry._getFacRelationsVanilla(pair));
            }
        }


    }


}
