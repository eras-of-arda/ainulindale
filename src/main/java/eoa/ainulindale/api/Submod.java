package eoa.ainulindale.api;

public abstract class Submod {

    public void loadSubmod() {};
    
    public void enableSubmod() {};
    
    public void disableSubmod() {};
    

}
