package eoa.ainulindale.api.event.registry;

import cpw.mods.fml.common.eventhandler.Event;
import eoa.ainulindale.api.AinulindaleAPI;

/**
 * The addon load event. This event is called on the {@link AinulindaleAPI#LOTR_EVENT_BUS}. During this event you should reverse any setup for your addon not handled directly by Ainulindalë (e.g. changing alignment icon, messing with the Star of Eärendil, etc.)
 * <p>
 * The event itself will be called upon addon loading, right after loadAddon runs.
 */
public class AddonUnloadEvent extends Event implements LOTREvent {

    public String addonID;

    public AddonUnloadEvent(String addonID)
    {
        this.addonID = addonID;
    }
}
