package eoa.ainulindale.api.event.registry;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.eventhandler.Event;
import eoa.ainulindale.api.AinulindaleAPI;
import eoa.ainulindale.api.registries.WaypointRegistry;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.map.LOTRWaypoint.Region;

/**
 * The waypoint registry event. This event is called on the {@link AinulindaleAPI#LOTR_EVENT_BUS}. During this event you should register your {@link LOTRWaypoint}s and {@link Region}s using the {@link WaypointRegistry}
 * <p>
 * The event itself will be called at the end of the LOTR {@link FMLInitializationEvent}
 */
public class WaypointRegistryEvent extends Event implements LOTREvent {

}
