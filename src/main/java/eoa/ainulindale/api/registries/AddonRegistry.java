package eoa.ainulindale.api.registries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.api.AinulindaleAPI;
import eoa.ainulindale.api.event.registry.AddonLoadEvent;
import eoa.ainulindale.api.event.registry.AddonUnloadEvent;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.util.resources.AddonProperties;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.LOTRShields;
import lotr.common.LOTRTitle;
import lotr.common.entity.LOTREntities;
import lotr.common.fac.LOTRFaction;
import lotr.common.fac.LOTRFactionRank;
import lotr.common.fac.LOTRFactionRelations;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.map.LOTRMountains;
import lotr.common.world.map.LOTRRoads;
import lotr.common.world.map.LOTRWaypoint;
import net.minecraft.util.ResourceLocation;

/**
 * The core addon registry.
 * <p>
 * This class is used to manage addons and get IDs for content that is not swapped in and out between addons.
 * Do not register your addon directly through this class. Instead, have your addon extend AinulindaleAddon and call setupAddon in your preInit method.
 */
public class AddonRegistry {

    private static String activeAddon = "lotr";

    private static int nextEntity = 0; //nextEntity function handles finding next free ID
    private static int nextCTID = 0;

    private static List<String> addons = new ArrayList<String>(); //this is the master list of loaded addon IDs

    private static Map<String, String> addonsForMapId = new HashMap<String, String>(); //links addon IDs to map IDs

    //private static Map<String, ResourceLocation> mapsForAddonId = new HashMap<String, ResourceLocation>();

    private static Map<String, AddonProperties> addonIDToData = new HashMap<String, AddonProperties>();

    public static AddonProperties getAddonProperties(String addonID)
    {
        return addonIDToData.get(addonID);
    }

    public static String getActiveAddon()
    {
        return activeAddon;
    }

    public static void registerAddon(String addonID)
    {
        addons.add(addonID);

        BiomeRegistry.nextBiome.put(addonID, 0);

        addonIDToData.put(addonID, new AddonProperties());
        if(!BiomeRegistry.biomeLists.containsKey(addonID)) {
            BiomeRegistry.biomeLists.put(addonID, new HashMap<Integer, LOTRBiome>());
        }
        if(!BiomeRegistry.biomeColorLists.containsKey(addonID)) {
            BiomeRegistry.biomeColorLists.put(addonID, new HashMap<Integer, Integer>());
        }
        if(!FactionRegistry.facLists.containsKey(addonID)) {
            FactionRegistry.facLists.put(addonID, new ArrayList<LOTRFaction>());
        }
        if(!FactionRegistry.relationsLists.containsKey(addonID)) {
            FactionRegistry.relationsLists.put(addonID, new HashMap<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation>());
        }
        if(!FactionRegistry.facRegionLists.containsKey(addonID)) {
            FactionRegistry.facRegionLists.put(addonID, new HashMap<String, LOTRDimension.DimensionRegion>());
        }
        if(!WaypointRegistry.regions.containsKey(addonID)) {
            WaypointRegistry.regions.put(addonID, new ArrayList<LOTRWaypoint.Region>());
        }
        if(!WaypointRegistry.waypoints.containsKey(addonID)) {
            WaypointRegistry.waypoints.put(addonID, new ArrayList<LOTRWaypoint>());
        }
        if(!HillRegistry.hills.containsKey(addonID)) {
            HillRegistry.hills.put(addonID, new ArrayList<LOTRMountains>());
        }
        if(!MapLabelRegistry.mapLabels.containsKey(addonID)) {
            MapLabelRegistry.mapLabels.put(addonID, new ArrayList<LOTRMapLabels>());
        }
        if(!RoadRegistry.roadsList.containsKey(addonID)) {
            RoadRegistry.roadsList.put(addonID, new ArrayList<LOTRRoads>());
        }
        if(!RoadRegistry.roadPointsLists.containsKey(addonID)) {
            RoadRegistry.roadPointsLists.put(addonID, new HashMap<LOTRRoads, LOTRRoads.RoadPoint[]>());
        }
        if(!ShieldRegistry.shields.containsKey(addonID)) {
            ShieldRegistry.shields.put(addonID, new ArrayList<LOTRShields>());
        }
        if(!TitleRegistry.titles.containsKey(addonID)) {
            TitleRegistry.titles.put(addonID, new ArrayList<LOTRTitle>());
        }
        if (!TitleRegistry.titlesForRank.containsKey(addonID)) {
            TitleRegistry.titlesForRank.put(addonID, new HashMap<LOTRFactionRank, List<LOTRTitle>>());
        }

    }

    public static String getAddonForMapID(String mapID)
    {
        return addonsForMapId.get(mapID);
    }

    public static ResourceLocation getMapIDForAddon(String addonID)
    {
        return addonIDToData.get(addonID).mapID;
    }

    public static void setAddonForMapID(ResourceLocation mapID, String addonID, MapRegistry.MapInfo mapInfo)
    {
        addonsForMapId.put(mapID.toString(), addonID);
        addonIDToData.get(addonID).mapID = mapID;
        addonIDToData.get(addonID).mapInfo = mapInfo;

    }

    /**
     * Get the next free entity ID
     *
     * @return the new entity ID.
     */
    public static int getNextEntity()
    {
        while(LOTREntities.getStringFromID(nextEntity) != null)
        {
            nextEntity++;
        }
        return nextEntity++;
    }

    /**
     * Get the next free crafting table GUI ID
     *
     * @return the new crafting table GUI ID.
     */
    public static int getNextCTID()
    {
        return nextCTID++;
    }

    /**
     * Loads an addon, swapping in all the content needed for that addon to run
     *
     * @param addonID the ID of the addon you want to load
     * @param theMap the map for the addon you want to load
     */
    public static void loadAddon(String addonID, ResourceLocation theMap)
    {
        String oldAddonID = activeAddon;
        if(activeAddon != null)
        {
            AinulindaleAPI.LOTR_EVENT_BUS.post(new AddonUnloadEvent(activeAddon));
        }
        activeAddon = addonID;
        AinulindaleAPI.LOTR_EVENT_BUS.post(new AddonLoadEvent(activeAddon));
        if(!addons.contains(addonID))
        {
            //fail
            Ainulindale.log.error("Addon "+addonID+" not found!");
        }
        else
        {
            Ainulindale.log.info("Loading addon "+addonID+"!");

            FactionRegistry.removeAllFactionsExcluding(); //clear facs

            for(int i = 0; i<256; i++) //wipe the entire biome list
            {
                LOTRBiome theBiome = LOTRDimension.MIDDLE_EARTH.biomeList[i];
                if(theBiome != null) {
                    LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.remove(theBiome.color);
                }
                LOTRDimension.MIDDLE_EARTH.biomeList[i] = null;
            }

            for(int id : BiomeRegistry.getBiomeList(addonID).keySet())
            {
                LOTRBiome biome = BiomeRegistry.getBiomeEntry(addonID, id);
                Ainulindale.log.info("Restoring biome "+biome.biomeName);
                LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.put(biome.color, biome.biomeID);
                LOTRDimension.MIDDLE_EARTH.biomeList[biome.biomeID] = biome;
            }

            LOTRDimension.MIDDLE_EARTH.dimensionRegions.clear();

            for(LOTRDimension.DimensionRegion region : FactionRegistry.getFactionRegionsList(addonID))
            {
                Ainulindale.log.info("Restoring faction region "+region.getRegionName());
                LOTRDimension.MIDDLE_EARTH.dimensionRegions.add(region);
            }


            for(LOTRFaction fac : FactionRegistry.facLists.get(addonID))
            {
                if(fac != LOTRFaction.HOSTILE && fac != LOTRFaction.UNALIGNED) {
                    FactionRegistry.activateFaction(fac);
                }
            }

            Map<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation> relations = new HashMap<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation>();

            for(LOTRFactionRelations.FactionPair pair : FactionRegistry.relationsLists.get(addonID).keySet())
            {
                relations.put(pair, FactionRegistry.relationsLists.get(addonID).get(pair));
            }

            ReflectionHelper.setPrivateValue(LOTRFactionRelations.class, null, relations,"defaultMap");

            for(LOTRWaypoint wp : LOTRWaypoint.values())
            {
                WaypointRegistry.clearWP(wp);
            }

            for(LOTRWaypoint wp : WaypointRegistry.getWaypoints(addonID))
            {
                WaypointRegistry.activateWP(wp);
            }

            if(oldAddonID == null)
            {
                oldAddonID = "lotr";
            }

            List<LOTRRoads> roadsToIterate = new ArrayList<LOTRRoads>(LOTRRoads.getAllRoadsInWorld());

            for(LOTRRoads road : roadsToIterate)
            {
                RoadRegistry.saveRoad(oldAddonID, road);
            }

            for(LOTRRoads road : RoadRegistry.getRoadsList(addonID))
            {
                RoadRegistry.restoreRoad(addonID, road);
            }

            for(LOTRMountains hill : LOTRMountains.values())
            {
                if((Float)(ReflectionHelper.getPrivateValue(LOTRMountains.class, hill, "height")) > 0)
                {
                    HillRegistry.saveHill(hill);
                }
            }

            for(LOTRMountains hill : HillRegistry.getHillList(addonID))
            {
                HillRegistry.restoreHill(hill);
            }


            for(LOTRMapLabels label : LOTRMapLabels.values())
            {
                MapLabelRegistry.hideMapLabel(label);
            }

            for(LOTRMapLabels label : MapLabelRegistry.getLabelList(addonID))
            {
                MapLabelRegistry.showMapLabel(label);
            }

            List<LOTRAchievement> achievesTemp = new ArrayList<LOTRAchievement>(LOTRDimension.MIDDLE_EARTH.allAchievements); //no concurrent modification

            for(LOTRAchievement achievement : achievesTemp)
            {
                AchievementRegistry.hideAchievement(oldAddonID, achievement);
            }

            List<LOTRAchievement.Category> achieveCategoriesTemp = new ArrayList<LOTRAchievement.Category>(LOTRDimension.MIDDLE_EARTH.achievementCategories);

            for(LOTRAchievement.Category achievementCategory : achieveCategoriesTemp)
            {
                AchievementRegistry.hideCategory(oldAddonID, achievementCategory);
            }

            for(LOTRTitle title : LOTRTitle.allTitles)
            {
                TitleRegistry.hideTitle(title);
            }

            for(LOTRTitle title : TitleRegistry.getTitles(addonID))
            {
                TitleRegistry.showTitle(title);
            }

            for(LOTRAchievement achievement : AchievementRegistry.getAchievementList(addonID))
            {
                AchievementRegistry.showAchievement(addonID, achievement);
            }

            for(LOTRAchievement.Category achievementCategory : AchievementRegistry.getCategoryList(addonID))
            {
                AchievementRegistry.showCategory(addonID, achievementCategory);
            }

            for(LOTRShields shield : LOTRShields.values())
            {
                ShieldRegistry.hideShield(shield);
            }

            for(LOTRShields shield : ShieldRegistry.getShields(addonID))
            {
                ShieldRegistry.restoreShield(shield);
            }


            MapRegistry.loadMap(theMap);

        }
    }

    /**
     * Loads an addon, swapping in all the content needed for that addon to run
     *
     * @param addonID the ID of the addon you want to load
     */
    public static void loadAddon(String addonID)
    {
        loadAddon(addonID, addonIDToData.get(addonID).mapID);
    }

    /**
     * Checks if an addon exists and is registered with a given ID
     *
     * @param addonID the ID of the addon you want to test for
     */
    public static boolean addonExistsWithName(String addonID)
    {
        return addons.contains(addonID);
    }

    public static List<String> getRegisteredAddons() {
        return addons;
    }
}
