package eoa.ainulindale.api.registries;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.Ainulindale;
import lotr.common.LOTRDimension;
import lotr.common.world.biome.LOTRBiome;

import java.util.HashMap;
import java.util.Map;


/**
 * The biome registry.
 * <p>
 * Use this class to register and or get any {@link LOTRBiome}. When creating or overriding a biome you should always use the methods in this class instead of manipulating the biome list directly.
 */
public class BiomeRegistry {

    static Map<String, Integer> nextBiome = new HashMap<String, Integer>();

    static Map<String, Map<Integer,LOTRBiome>> biomeLists = new HashMap<String, Map<Integer,LOTRBiome>>();

    static Map<String, Map<Integer, Integer>> biomeColorLists = new HashMap<String, Map<Integer, Integer>>();

    /**
     * Gets the biome list for an addon
     *
     * @param addonID the addon ID
     * @return the biome list for the addon.
     */
    public static Map<Integer, LOTRBiome> getBiomeList(String addonID) {
        Ainulindale.log.info("LOTR biome list now has "+BiomeRegistry.biomeLists.get("lotr").keySet().size()+" entries");
        Ainulindale.log.info("addon biome list now has "+BiomeRegistry.biomeLists.get(addonID).keySet().size()+" entries");
        return biomeLists.get(addonID);
    }

    /**
     * Checks if a biome list is set up for an addon
     *
     * @param addonID the addon ID
     * @return is there an entry in biomeList for this addon
     */
    public static boolean checkBiomeListRegistered(String addonID) {
        return biomeLists.containsKey(addonID);
    }


    /**
     * Gets the biome for a given ID for an addon
     *
     * @param addonID the addon ID
     * @param id the biome ID
     * @return the biome.
     */
    public static LOTRBiome getBiomeEntry(String addonID, int id) {
        //TODO: error checking
        return biomeLists.get(addonID).get(id);
    }

    /**
     * Sets an ID and color to a certain biome for an addon
     *
     * @param addonID the addon ID
     * @param id the biome ID
     * @param biome the biome
     * @param color the color
     */
    public static void setBiomeEntry(String addonID, int id, LOTRBiome biome, int color) {
        //TODO: error checking
        if(!checkBiomeListRegistered(addonID))
        {
            Ainulindale.log.debug("Creating biome list for addon "+addonID);
            BiomeRegistry.biomeLists.put(addonID, new HashMap<Integer, LOTRBiome>());
            BiomeRegistry.biomeColorLists.put(addonID, new HashMap<Integer, Integer>());
        }
        Ainulindale.log.debug("Saving biome "+biome.biomeName+" ("+id+") for addon "+addonID);
        BiomeRegistry.biomeLists.get(addonID).put(id, biome);
        BiomeRegistry.biomeColorLists.get(addonID).put(color, id);
        Ainulindale.log.debug("biome list now has "+BiomeRegistry.biomeLists.get(addonID).keySet().size()+" entries");
    }


    /**
     * Gets the biome color map for an addon ID
     *
     * @param addonID the addon ID
     * @return the addon's color map.
     */
    public static Map<Integer, Integer> getBiomeColorList(String addonID) {
        //TODO: error checking
        return biomeColorLists.get(addonID);
    }

    /**
     * Gets the biome ID for a color and addon ID
     *
     * @param addonID the addon ID
     * @param color the biome color
     * @return the matching biome ID.
     */
    public static Integer getBiomeColorEntry(String addonID, int color) {
        //TODO: error checking
        return biomeColorLists.get(addonID).get(color);
    }


    /**
     * Get the next free biome ID for the given addon ID.
     *
     * @param addonID the addon ID
     * @return the new biome ID.
     */
    public static int getNextBiome(String addonID, Integer color)
    {
        while(BiomeRegistry.biomeLists.get(addonID).containsKey(nextBiome.get(addonID)))
        {
            nextBiome.put(addonID, nextBiome.get(addonID)+1);
        }
        if(LOTRDimension.MIDDLE_EARTH.biomeList[nextBiome.get(addonID)] != null)
        {
            LOTRDimension.MIDDLE_EARTH.biomeList[nextBiome.get(addonID)] = null;
        }
        if(LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.containsKey(color))
        {
            LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.remove(color);
        }
        return nextBiome.get(addonID);
    }

    /**
     * Replaces an existing biome with a new biome.
     *
     * @param addonID the ID of the addon performing the replacement
     * @param oldBiome the biome to replace
     * @param newBiome the biome to replace oldBiome with
     */
    public static void replaceBiome(String addonID, LOTRBiome oldBiome, LOTRBiome newBiome)
    {
        if(newBiome != null)
        {
            throw new IllegalArgumentException("newBiome cannot be null");
        }
        if(oldBiome != null)
        {
            throw new IllegalArgumentException("oldBiome cannot be null");
        }
        if(!AddonRegistry.addonExistsWithName(addonID))
        {
            throw new IllegalArgumentException("addon '"+addonID+"' does not exist");
        }
        int oldNewId = newBiome.biomeID;
        int oldNewColor = newBiome.color;
        ReflectionHelper.setPrivateValue(LOTRBiome.class, newBiome, oldBiome.biomeID, "biomeID");
        newBiome.color = oldBiome.color;
        if(AddonRegistry.getActiveAddon() == addonID)
        {
            LOTRDimension.MIDDLE_EARTH.biomeList[oldNewId] = null;
            LOTRDimension.MIDDLE_EARTH.biomeList[oldBiome.biomeID] = newBiome;
            LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.remove(oldNewColor);
            LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.put(newBiome.color, newBiome.biomeID);
        }
        biomeLists.get(addonID).remove(oldNewId);
        biomeLists.get(addonID).put(newBiome.biomeID, newBiome);
        biomeColorLists.get(addonID).remove(oldNewColor);
        biomeColorLists.get(addonID).put(newBiome.color, newBiome.biomeID);

    }

}
