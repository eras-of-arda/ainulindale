package eoa.ainulindale.api.registries;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.reflection.LOTREnumHelpers;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import eoa.ainulindale.common.util.resources.FactionProperties;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.fac.*;

import java.util.*;

/**
 * The faction registry.
 * <p>
 * Use this class to register and or get any {@link LOTRFaction}. When adding or removing a faction you should always use this class instead of directly using {@link LOTRFaction}.
 */
public class FactionRegistry {


    static Map<String, List<LOTRFaction>> facLists = new HashMap<String, List<LOTRFaction>>();

    static Map<String, Map<String, LOTRDimension.DimensionRegion>> facRegionLists = new HashMap<String, Map<String, LOTRDimension.DimensionRegion>>();

    static Map<String, FactionProperties> factionProperties = new HashMap<String, FactionProperties>();

    static Map<String, Map<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation>> relationsLists = new HashMap<String, Map<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation>>();

    /**
     * Checks if a faction is in an addon
     *
     * @param addonID the addon ID
     * @param faction the faction to check for
     * @return whether the faction is in the addon
     */
    public static boolean isFacInAddon(String addonID, LOTRFaction faction)
    {
        return facLists.get(addonID).contains(faction);
    }

    /**
     * Internal method, returns a list of vanilla LOTRMod factions
     *
     * @return vanilla LOTRMod factions
     */
    public static List<LOTRFaction> _getVanillaFactions()
    {
        return facLists.get("lotr");
    }

    /**
     * Internal method, adds a faction for the vanilla LOTRMod
     *
     * @param fac the faction
     */
    public static void _addVanillaFaction(LOTRFaction fac)
    {
        addFactionToList("lotr", fac);
    }

    /**
     * Sets the faction relations for the given addon ID for a given faction pair
     *
     * @param addonID the addon ID
     * @param pair the faction pair to get relations for
     */
    public static void setFacRelations(String addonID, LOTRFactionRelations.FactionPair pair, LOTRFactionRelations.Relation relation)
    {
        if(!relationsLists.containsKey(addonID)) relationsLists.put(addonID, new HashMap<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation>());
        relationsLists.get(addonID).put(pair, relation);
    }

    /**
     * Internal method, sets the faction relations for the base LOTRMod for a given faction pair
     *
     * @param pair the faction pair to get relations for
     */
    public static void _setFacRelationsVanilla(LOTRFactionRelations.FactionPair pair, LOTRFactionRelations.Relation relation)
    {
        setFacRelations("lotr", pair, relation);
    }

    /**
     * Internal method, gets the faction relations for the base LOTRMod for a given faction pair
     *
     * @param pair the faction pair to get relations for
     * @return the vanilla faction relations for the faction
     */
    public static LOTRFactionRelations.Relation _getFacRelationsVanilla(LOTRFactionRelations.FactionPair pair)
    {
        return getFacRelations("lotr", pair);
    }


    /**
     * Gets the faction relations for the base LOTRMod for a given faction pair and addon ID
     * @param addonID the addon ID to get the faction relations for
     * @param pair the faction pair to get relations for
     * @return the vanilla faction relations for the faction
     */
    public static LOTRFactionRelations.Relation getFacRelations(String addonID, LOTRFactionRelations.FactionPair pair)
    {
        return relationsLists.get(addonID).get(pair);
    }

    /**
     * Internal method, gets the list of faction relations for the base LOTRMod
     *
     * @return the faction relations list for the vanilla LOTRMod
     */
    public static Map<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation> _getFacRelationsListVanilla()
    {
        return getFacRelationsList("lotr");
    }

    /**
     * Internal method, gets the list of faction relations for the given addon ID
     *
     * @param addonID the addon ID to get the faction relations list for
     * @return the faction relations list for the vanilla LOTRMod
     */
    public static Map<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation> getFacRelationsList(String addonID)
    {
        return relationsLists.get(addonID);
    }


    public static LOTRFaction addFactionToList(String addonID, LOTRFaction theFac)
    {
        if(!facLists.containsKey(addonID)) facLists.put(addonID, new ArrayList<LOTRFaction>());
        facLists.get(addonID).add(theFac);
        return theFac;
    }

    public static void addFactionRegion(String addonID, String regionName, LOTRDimension dim)
    {
        LOTRDimension.DimensionRegion region = LOTREnumHelpers.addDimensionRegion((addonID+"_"+regionName).toUpperCase(Locale.ROOT), addonID+":"+regionName);
        region.setDimension(dim);
        dim.dimensionRegions.add(region);
        if(!facRegionLists.containsKey(addonID)) facRegionLists.put(addonID, new HashMap<String, LOTRDimension.DimensionRegion>());
        facRegionLists.get(addonID).put((addonID+"_"+regionName).toUpperCase(Locale.ROOT), region);
    }

    public static void addFactionRegionAsObject(String addonID, String regionName, LOTRDimension.DimensionRegion region)
    {
        if(!facRegionLists.containsKey(addonID)) facRegionLists.put(addonID, new HashMap<String, LOTRDimension.DimensionRegion>());
        facRegionLists.get(addonID).put(regionName, region);
    }

    public static Map<String, LOTRDimension.DimensionRegion> getFactionRegions(String addonID)
    {
        return facRegionLists.get(addonID);
    }

    public static List<LOTRDimension.DimensionRegion> getFactionRegionsList(String addonID)
    {
        if(facRegionLists.get(addonID) == null)
        {
            return null;
        }
        return new ArrayList<LOTRDimension.DimensionRegion>(facRegionLists.get(addonID).values());
    }

    public static LOTRDimension.DimensionRegion getFactionRegion(String addonID, String regionName)
    {
        return facRegionLists.get(addonID).get((addonID+"_"+regionName).toUpperCase(Locale.ROOT));
    }

    /**
     * Register a new {@link LOTRFaction} to the faction registry. Usually this method will be called in the addon mod's preInit method.
     *
     * @param addonID the ID of the addon you want to register it to, usually the same as your addon mod's ID.
     * @param name the faction enum name
     * @param color the faction alignment bar color
     * @param region the DimensionRegion the faction should be in
     * @param types the faction's type(s)
     * @param category the achievement category the faction should use (can be null unless you are planning to add any achievements)
     * @return the newly registered faction.
     */
    public static LOTRFaction addFaction(String addonID, String name, int color, LOTRDimension.DimensionRegion region, EnumSet types, LOTRAchievement.Category category)
    {
        LOTRFaction theFac = LOTREnumHelpers.addFaction(addonID+":"+name, color, region, types);
        facLists.get(addonID).add(theFac);
        LOTRReflectionHelpers.setFactionAchievementCategory(theFac, category);
        clearFaction(theFac); //fac not active yet
        return theFac;
    }

    /**
     * Register a new {@link LOTRFaction} to the faction registry. Usually this method will be called in the addon mod's preInit method.
     *
     * @param addonID the ID of the addon you want to register it to, usually the same as your addon mod's ID.
     * @param name the faction enum name
     * @param color the faction alignment bar color
     * @param dim the LOTRDimension the faction should be in
     * @param region the DimensionRegion the faction should be in
     * @param player the value of the faction's allowPlayer flag
     * @param registry the value of the faction's allowEntityRegistry flag
     * @param alignment the faction's fixed alignment. Set to Integer.MIN_VALUE for no fixed alignment
     * @param mapInfo the faction's GUI map display area, as a FactionMapInfo object
     * @param types the faction's type(s)
     * @param category the achievement category the faction should use (can be null unless you are planning to add any achievements)
     * @return the newly registered faction.
     */
    public static LOTRFaction addFaction(String addonID, String name, int color, LOTRDimension dim, LOTRDimension.DimensionRegion region, boolean player, boolean registry, int alignment, LOTRMapRegion mapInfo, EnumSet types, LOTRAchievement.Category category)
    {
        LOTRFaction theFac = LOTREnumHelpers.addFaction(addonID+":"+name, color, dim, region, player, registry, alignment, mapInfo, types);
        facLists.get(addonID).add(theFac);
        LOTRReflectionHelpers.setFactionAchievementCategory(theFac, category);
        clearFaction(theFac); //fac not active yet
        return theFac;
    }

    /**
     * Get an {@link LOTRFaction} object from the registry if it exists
     *
     * @param addonID the ID of the addon the faction is from
     * @param name the faction enum name
     * @return the {@link LOTRFaction} object for the specified faction, or null if it does not exist.
     */
    public static LOTRFaction getFaction(String addonID, String name)
    {
        List<LOTRFaction> facs = facLists.get(addonID);
        for(LOTRFaction fac : facs)
        {
            if(fac.name() == addonID+":"+name)
            {
                return fac;
            }
        }
        return null;
    }

    /**
     * Get the list of {@link LOTRFaction} instances for an addon ID
     *
     * @param addonID the ID of the addon the faction is from
     * @return the list of {@link LOTRFaction} objects for the addon
     */
    public static List<LOTRFaction> getFactionList(String addonID)
    {
        return facLists.get(addonID);

    }

    static void removeAllFactionsExcluding(LOTRFaction... facs)
    {
        for(LOTRFaction fac : LOTRFaction.values())
        {
            for(LOTRFaction fac2 : facs) {
                if(fac2 == fac)
                {
                    continue;
                }
            }
            if(fac.allowPlayer || !fac.hasFixedAlignment) {
                clearFaction(fac);
            }
        }
    }

    public static void addFactionRank(String addonID, LOTRFaction faction, float alignment, String name) {
        addFactionRank(addonID, faction, alignment, name, false, true, true);
    }

    public static void addFactionRank(String addonID, LOTRFaction faction, float alignment, String name, boolean gendered) {
        addFactionRank(addonID, faction, alignment, name, gendered, true, true);
    }

    public static void addFactionRank(String addonID, LOTRFaction faction, float alignment, String name, boolean gendered, boolean makeAchieve, boolean makeTitle) {
        LOTRFactionRank rank = LOTRReflectionHelpers.addFactionRank(faction, alignment, name, gendered);
        if(makeAchieve) {
            rank = rank.makeAchievement();
            AchievementRegistry.registerAchievement(addonID, rank.getRankAchievement());
        }
        if(makeTitle) {
            rank = rank.makeTitle();
            if(gendered)
            {
                TitleRegistry.registerTitle(addonID, ReflectionHelper.getPrivateValue(LOTRFactionRank.class, rank, "rankTitleMasc"));
                TitleRegistry.registerTitle(addonID, ReflectionHelper.getPrivateValue(LOTRFactionRank.class, rank, "rankTitleFem"));
            }
            else
            {
                TitleRegistry.registerTitle(addonID, ReflectionHelper.getPrivateValue(LOTRFactionRank.class, rank, "rankTitle"));
            }
        }
    }

    public static void setFactionPledgeRank(LOTRFaction faction, LOTRFactionRank rank) {
        if(faction.getPledgeRank() == null)
        {
            faction.setPledgeRank(rank);
        }
        else
        {
            if(rank.fac != faction)
            {
                throw new IllegalArgumentException("Rank is set to a different faction!");
            }
            ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, rank, "pledgeRank");
        }
    }

    static void clearFaction(LOTRFaction faction) {

        System.out.println("Saving faction: "+faction.name());

        FactionProperties props = new FactionProperties();
        props.allowPlayer = faction.allowPlayer;
        props.hasFixedAlignment = faction.hasFixedAlignment;
        props.fixedAlignment = faction.fixedAlignment;
        props.factionDimension = faction.factionDimension;
        if(faction.factionDimension != null)
        {
            System.out.println("dim: "+faction.factionDimension.dimensionName);
        }
        else
        {
            System.out.println("dim: NULL");
        }
        props.factionRegion = faction.factionRegion;
        props.controlZones = new ArrayList<LOTRControlZone>();
        for(LOTRControlZone zone : faction.getControlZones())
        {
            props.controlZones.add(zone);
        }
        props.allowEntityRegistry = faction.allowEntityRegistry;
        props.approvesOfWarCrimes = faction.approvesWarCrimes;
        props.banners = faction.factionBanners;
        props.category = faction.getAchieveCategory();
        props.color = ReflectionHelper.getPrivateValue(LOTRFaction.class, faction, "factionColor");
        props.ranks = ReflectionHelper.getPrivateValue(LOTRFaction.class, faction, "ranksSortedDescending");
        props.factionTypes = ReflectionHelper.getPrivateValue(LOTRFaction.class, faction, "factionTypes");
        props.name = faction.factionName();
        props.pledgeRank = faction.getPledgeRank();

        FactionRegistry.factionProperties.put(faction.name(), props);

        faction.allowPlayer = false;
        faction.hasFixedAlignment = true;
        faction.fixedAlignment = 0;

        if(faction.factionDimension != null) faction.factionDimension.factionList.remove(faction);
        if(faction.factionRegion != null) faction.factionRegion.factionList.remove(faction);
        faction.factionDimension = null;
        faction.factionRegion = null;
        LOTRReflectionHelpers.clearControlZones(faction);
    }

    static void activateFaction(LOTRFaction fac)
    {
        System.out.println("Trying to restore faction "+fac.name());
        if(FactionRegistry.factionProperties.containsKey(fac.name()))
        {
            fac.allowPlayer = FactionRegistry.factionProperties.get(fac.name()).allowPlayer;
            fac.hasFixedAlignment = FactionRegistry.factionProperties.get(fac.name()).hasFixedAlignment;
            fac.fixedAlignment = FactionRegistry.factionProperties.get(fac.name()).fixedAlignment;
            fac.factionDimension = FactionRegistry.factionProperties.get(fac.name()).factionDimension;
            fac.factionRegion = FactionRegistry.factionProperties.get(fac.name()).factionRegion;
            if(fac.factionDimension != null) {
                fac.factionDimension.factionList.add(fac);
                System.out.println("Restoring dim for "+fac.name());
            }
            else
            {
                System.out.println("Not restoring NULL dim for "+fac.name());
            }
            if(fac.factionRegion != null) {
                fac.factionRegion.factionList.add(fac);
                System.out.println("Restoring region for "+fac.name());
            }
            else
            {
                System.out.println("Not restoring NULL region for "+fac.name());
            }
            ReflectionHelper.setPrivateValue(LOTRFaction.class, fac, FactionRegistry.factionProperties.get(fac.name()).factionTypes,"factionTypes");
            ReflectionHelper.setPrivateValue(LOTRFaction.class, fac, FactionRegistry.factionProperties.get(fac.name()).name,"factionName");
            ReflectionHelper.setPrivateValue(LOTRFaction.class, fac, FactionRegistry.factionProperties.get(fac.name()).color,"factionColor");
            ReflectionHelper.setPrivateValue(LOTRFaction.class, fac, FactionRegistry.factionProperties.get(fac.name()).ranks,"ranksSortedDescending");
            ReflectionHelper.setPrivateValue(LOTRFaction.class, fac, FactionRegistry.factionProperties.get(fac.name()).pledgeRank,"pledgeRank");
            fac.factionMapInfo = FactionRegistry.factionProperties.get(fac.name()).mapInfo;
            fac.isolationist = FactionRegistry.factionProperties.get(fac.name()).isolationist;
            fac.approvesWarCrimes = FactionRegistry.factionProperties.get(fac.name()).approvesOfWarCrimes;
            fac.factionBanners = FactionRegistry.factionProperties.get(fac.name()).banners;
            fac.allowEntityRegistry = FactionRegistry.factionProperties.get(fac.name()).allowEntityRegistry;
            ReflectionHelper.setPrivateValue(LOTRFaction.class, fac, FactionRegistry.factionProperties.get(fac.name()).category,"achieveCategory");
            for(LOTRControlZone zone : FactionRegistry.factionProperties.get(fac.name()).controlZones)
            {
                LOTRReflectionHelpers.addControlZone(fac, zone);
            }
        }
        else
        {
            System.out.println("fatal: faction not found: "+fac.name());
        }
    }
}
