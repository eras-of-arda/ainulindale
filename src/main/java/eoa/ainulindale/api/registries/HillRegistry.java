package eoa.ainulindale.api.registries;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.reflection.LOTREnumHelpers;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import eoa.ainulindale.common.util.resources.FixedHillProperties;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.map.LOTRMountains;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The fixed hill registry.
 * <p>
 * Use this class to register and or get any {@link LOTRMountains}. When adding a fixed hill you should always use this class instead of directly manipulating the enum.
 */
public class HillRegistry {
    static Map<String, List<LOTRMountains>> hills = new HashMap<String, List<LOTRMountains>>();
    public static Map<LOTRMountains, FixedHillProperties> hillData = new HashMap<LOTRMountains, FixedHillProperties>();

    /**
     * Creates a new {@link LOTRMountains} instance for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this hill
     * @param name the name of the new hill you are creating
     * @param x the X coordinate (in map pixels) of the hill
     * @param y the X coordinate (in map pixels) of the hill
     * @param height the height of the hill
     * @param range the width of the hill
     */
    public static LOTRMountains addHill(String addonID, String name, float x, float y, float height, int range)
    {
        return addHill(addonID, name, x, y, height, range, 0);
    }

    /**
     * Creates a new {@link LOTRMountains} instance for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this hill
     * @param name the name of the new hill you are creating
     * @param x the X coordinate (in map pixels) of the hill
     * @param y the X coordinate (in map pixels) of the hill
     * @param height the height of the hill
     * @param range the width of the hill
     * @param lavaRange the width of the hill's lava crater, set to 0 for no lava crater
     */
    public static LOTRMountains addHill(String addonID, String name, float x, float y, float height, int range, int lavaRange)
    {
        LOTREnumHelpers.addMountain(addonID+":"+name, x, y, height, range, lavaRange);
        if(!hills.containsKey(addonID)) hills.put(addonID, new ArrayList<LOTRMountains>());
        hills.get(addonID).add(LOTRMountains.valueOf(addonID+":"+name));
        return LOTRMountains.valueOf(addonID+":"+name);
    }

    /**
     * Registers an {@link LOTRMountains} instance for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this hill
     * @param hill the {@link LOTRMountains} object you wish to register
     */
    public static void addHill(String addonID, LOTRMountains hill)
    {
        if(!hills.containsKey(addonID)) hills.put(addonID, new ArrayList<LOTRMountains>());
        hills.get(addonID).add(hill);
    }

    /**
     * Deactivate a given {@link LOTRMountains} for the current active addon.
     *
     * @param hill the hill to deactivate
     */
    static void saveHill(LOTRMountains hill)
    {
        FixedHillProperties props = new FixedHillProperties();
        props.x = ReflectionHelper.getPrivateValue(LOTRMountains.class, hill, "xCoord");
        props.z = ReflectionHelper.getPrivateValue(LOTRMountains.class, hill, "zCoord");
        props.height = ReflectionHelper.getPrivateValue(LOTRMountains.class, hill, "height");
        props.width = ReflectionHelper.getPrivateValue(LOTRMountains.class, hill, "range");
        props.lavaRange = ReflectionHelper.getPrivateValue(LOTRMountains.class, hill, "lavaRange");
        hillData.put(hill, props);
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, 0,"xCoord");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, 0,"zCoord");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, 0,"height");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, 0,"range");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, 0,"lavaRange");
    }

    /**
     * Reactivate a given {@link LOTRMountains} for the current active addon.
     *
     * @param hill the hill to reactivate
     */
    static void restoreHill(LOTRMountains hill)
    {
        FixedHillProperties props = hillData.get(hill);
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, props.x,"xCoord");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, props.z,"zCoord");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, props.height,"height");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, props.width,"range");
        ReflectionHelper.setPrivateValue(LOTRMountains.class, hill, props.lavaRange,"lavaRange");
    }

    /**
     * Return the list of {@link LOTRMountains} entries for the given addon ID.
     *
     * @param addonID the ID of the addon to get the list for
     * @return the list of hills for the addon given
     */
    public static List<LOTRMountains> getHillList(String addonID)
    {
        if(!hills.containsKey(addonID)) hills.put(addonID, new ArrayList<LOTRMountains>());
        return hills.get(addonID);
    }
}
