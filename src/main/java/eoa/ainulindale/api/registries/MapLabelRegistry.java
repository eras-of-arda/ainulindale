package eoa.ainulindale.api.registries;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.reflection.LOTREnumHelpers;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import eoa.ainulindale.common.util.resources.MapLabelProperties;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.world.biome.LOTRBiome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The map label registry.
 * <p>
 * Use this class to register and or get any {@link LOTRMapLabels}. When adding or removing a map label you should always use this class instead of directly using {@link LOTRMapLabels}.
 */
public class MapLabelRegistry {
    static Map<String, List<LOTRMapLabels>> mapLabels = new HashMap<String, List<LOTRMapLabels>>();
    static Map<LOTRMapLabels, MapLabelProperties> mapLabelData = new HashMap<LOTRMapLabels, MapLabelProperties>();

    /**
     * Creates a new {@link LOTRMapLabels} instance for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this label
     * @param biomeOrName the name of the new map label you are creating, or the name of a biome to link it to
     * @param f the size of the map label you are creating
     * @param r the rotation angle of the map label you are creating
     * @param z1 the minimum zoom level at which the label will be visible
     * @param z2 the maximum zoom level at which the label will be visible
     */
    public static void addMapLabel(String addonID, Object biomeOrName, int x, int y, float f, int r, float z1, float z2)
    {
        LOTRMapLabels label;
        if(biomeOrName instanceof LOTRBiome) {
            label = LOTREnumHelpers.addMapLabel(((LOTRBiome) biomeOrName).biomeName, (LOTRBiome)biomeOrName, x, y, f, r, z1, z2);
        }
        else
        {
            label = LOTREnumHelpers.addMapLabel(((String) biomeOrName), (String)biomeOrName, x, y, f, r, z1, z2);
        }
        if(!mapLabels.containsKey(addonID)) mapLabels.put(addonID, new ArrayList<LOTRMapLabels>());
        mapLabels.get(addonID).add(label);
    }

    /**
     * Registers an {@link LOTRMapLabels} instance for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this hill
     * @param label the map label to register
     */
    public static void registerMapLabel(String addonID, LOTRMapLabels label)
    {
        Ainulindale.log.info("registering map label "+label.name()+" for addon "+addonID);
        if(!mapLabels.containsKey(addonID)) mapLabels.put(addonID, new ArrayList<LOTRMapLabels>());
        mapLabels.get(addonID).add(label);
    }

    /**
     * Gets the list of {@link LOTRMapLabels} instances for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this hill
     * @return the list of map labels
     */
    public static List<LOTRMapLabels> getLabelList(String addonID)
    {
        if(!mapLabels.containsKey(addonID)) mapLabels.put(addonID, new ArrayList<LOTRMapLabels>());
        return mapLabels.get(addonID);
    }

    /**
     * Hides the given {@link LOTRMapLabels} instance for the current active addon
     *
     * @param label the map label to hide
     */
    static void hideMapLabel(LOTRMapLabels label)
    {
        Ainulindale.log.info("hiding map label "+label.name());
        if(!mapLabelData.containsKey(label)) {
            MapLabelProperties props = new MapLabelProperties();
            props.minZoom = label.minZoom;
            props.maxZoom = label.maxZoom;
            mapLabelData.put(label, props);
        }
        LOTRReflectionHelpers.removeMapLabel(label);
    }

    /**
     * Show the given {@link LOTRMapLabels} instance for the current active addon
     *
     * @param label the map label to show
     */
    static void showMapLabel(LOTRMapLabels label)
    {
        MapLabelProperties props = mapLabelData.get(label);
        Ainulindale.log.info("showing map label "+label.name()+" maxZoom: "+props.maxZoom+" minZoom: "+props.minZoom);
        ReflectionHelper.setPrivateValue(LOTRMapLabels.class, label, props.maxZoom, "maxZoom");
        ReflectionHelper.setPrivateValue(LOTRMapLabels.class, label, props.minZoom, "minZoom");
    }

}
