package eoa.ainulindale.api.registries;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import com.google.common.collect.ImmutableBiMap;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.ainulindale.client.HiddenBiomeCacheClient;
import eoa.ainulindale.client.TextureUtils;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import eoa.ainulindale.common.reflection.ReflectionUtils;
import eoa.ainulindale.common.util.resources.InputStreamUtils;
import eoa.ainulindale.common.util.resources.ResourceHelper;
import eoa.ainulindale.common.world.biome.HiddenBiome;
import lotr.common.LOTRDimension;
import lotr.common.LOTRModInfo;
import lotr.common.world.biome.LOTRBiome;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

/**
 * The map registry.
 * <p>
 * Use this class to register and or load/active any maps. Maps are stored by id which is a {@link ResourceLocation}. Usually this ID is the path to the map file but that isn't always the case for {@link File} based maps.
 * <p>
 * The normal LOTR mod doesn't support multiple maps so some core modding has been done to make this possible.
 */
public class MapRegistry {
    public static ResourceLocation defaultMap;
    
    private static Map<ResourceLocation, MapInfo> registeredMaps = new LinkedHashMap<ResourceLocation, MapInfo>();
    private static MapInfo activeMap;
    
    
    /**
     * Register a new {@link MapInfo} to the map registry. Usually this method will be called during {@link FMLPreInitializationEvent} or {@link FMLInitializationEvent}. It is recommended that all maps are registered in startup but it can also be done afterwards.
     * <p>
     * Note: registering a map doesn't make it the active map, this should be done with {@link #setActiveMap(ResourceLocation)}
     * 
     * @param map the map info which is going to be registered.
     */
    public static void registerMap(MapInfo map) {
        if(registeredMaps.containsKey(map.id)) {
            Ainulindale.log.warn("Trying to register map " + map.id + " but that map is already registered, overriding.");
        }
        
        registeredMaps.put(map.id, map);
    }
    
    /**
     * Set a map as the currently active map but it doesn't load it.
     * <p>
     * Note: at the end of startup the currently active map will automatically be loaded. If you're calling this method after startup you will probably also want to load it with {@link #loadMap(ResourceLocation)} which in turn also sets it as the active map.
     * 
     * @param id the map id.
     */
    public static void setActiveMap(ResourceLocation id) {
        MapInfo info = registeredMaps.get(id);
        
        if(info == null) {
            Ainulindale.log.warn("Tried activate map " + id + " but found no map for that id, skipping.");
            return;
        }
        
        if(activeMap != null && activeMap != info) activeMap.unloadMap();
        activeMap = info;
    }
    
    /**
     * Loads a map and saves the needed biome data. This method will fill set the request map as the active map.
     * <p>
     * Note: at the end of startup the currently active map will automatically be loaded.
     * 
     * @param id the map id.
     */
    public static void loadMap(ResourceLocation id) {
        setActiveMap(id);
        getActiveMap().loadServerMap();
    }

    /**
     * Reloads the client data of the currently active map.
     */
    public static void reloadActiveMap() {
        activeMap.reloadClientMap();
    }

    /**
     * Get the currently active {@link MapInfo}.
     * 
     * @return the active map
     */
    public static MapInfo getActiveMap() {
        return activeMap;
    }
    
   /**
    * Get a immutable map with a fixed iteration order of all the registered maps.
    * 
    * @return the registered maps
    */
   public static Map<ResourceLocation, MapInfo> getRegisteredMaps() {
       return new ImmutableBiMap.Builder<ResourceLocation, MapInfo>().putAll(registeredMaps).build();
   }
   
   /**
    * Checks if the given {@link ResourceLocation} id is corresponds to a existing map id.
    * 
    * @param id the id
    * @return if the map exists
    */
   public static boolean doesMapExist(ResourceLocation id) {
       return registeredMaps.keySet().contains(id);
   }
    
    static {
        // Register the default lotr map
        MapRegistry.defaultMap = new ResourceLocation(LOTRModInfo.modID, "map/map.png");
        MapInfo defaultMap = new SimpleMapInfo(MapRegistry.defaultMap);
        registerMap(defaultMap);
        AddonRegistry.setAddonForMapID(defaultMap.getID(), "lotr", defaultMap);
        setActiveMap(defaultMap.getID());
    }
    
    /**
     * A wrapper class which contains the client and server map data.
     * <p>
     * If you need a simple implementation of this class see {@link SimpleMapInfo}
     */
    public abstract static class MapInfo {
        // ID
        private final ResourceLocation id;
        
        // Client data
        @SideOnly(value = Side.CLIENT)
        private ResourceLocation mapTexture;
        @SideOnly(value = Side.CLIENT)
        private ResourceLocation sapiaMapTexture;
        @SideOnly(value = Side.CLIENT)
        private static ClientMapThread clientMapGenThread;

        // Server data
        private byte[] biomeData = new byte[0];
        private int imgWidth;
        private int imgHeight;
        
        public MapInfo(ResourceLocation id) {
            this.id = id;
        }
        
        /**
         * Get the the map id.
         * 
         * @return the map id.
         */
        public ResourceLocation getID() {
            return id;
        }
        
        /**
         * Get the the map id.
         * 
         * @return the map id.
         */
        public String getDisplayName() {
            return StatCollector.translateToLocal("mapregistry." + id.getResourceDomain() + "." + id.getResourcePath().substring(0, id.getResourcePath().length() - 4).replace("/", ".") + ".name");
        }
        
        /**
         * Get the the map icon as a {@link ResourceLocation}
         * 
         * @return the map icon.
         */
        @SideOnly(value = Side.CLIENT)
        public abstract ResourceLocation getIcon();
        
        /**
         * Override this method if you want to provide the client map.
         * 
         * @return the map
         */
        @SideOnly(value = Side.CLIENT)
        protected abstract BufferedImage generateClientMap() throws IOException;
        
        /**
         * Override this method if you want to provide the server map used for biome generation.
         * 
         * @return the map
         */
        protected abstract BufferedImage generateServerMap() throws IOException;
        
        private void loadServerMap() {
            if(biomeData.length != 0) {
                Ainulindale.log.warn("Tried loading map " + id + " but that map is already loaded, skipping.");
                return;
            }
            
            BufferedImage img = null;
            try {
                img = generateServerMap();
            }
            catch(IOException e) {
                Ainulindale.log.error("Failed to load server map for " + id);
                e.printStackTrace();
            }
            
            imgWidth = img.getWidth();
            imgHeight = img.getHeight();

            int[] colors = img.getRGB(0, 0, imgWidth, imgHeight, null, 0, imgWidth);
            biomeData = new byte[imgWidth * imgHeight];

            for(int i = 0; i < colors.length; ++i) {
                int color = colors[i];
                Integer biomeID = LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.get(color);
                
                if(biomeID == null) {
                    Ainulindale.log.error("Found unknown biome on map: " + Integer.toHexString(color) + " at location: " + (i % imgWidth) + ", " + (i / imgWidth));
                    biomeData[i] = (byte) LOTRBiome.ocean.biomeID;
                    continue;
                }

                biomeData[i] = (byte) biomeID.intValue();
            }
            
            LOTRReflectionHelpers.LOTRMap.setBiomeData(biomeData, imgWidth, imgHeight);
        }
        
        private void unloadMap() {
            if(biomeData.length == 0) {
                Ainulindale.log.warn("Tried unloading map " + id + " but that map is already unloaded, skipping.");
                return;
            }
            
            imgWidth = -1;
            imgHeight = -1;
            biomeData = new byte[0];
            
            if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
                Map<ResourceLocation, ITextureObject> textureMap = ReflectionUtils.Fields.getPrivateValue(TextureManager.class, Minecraft.getMinecraft().renderEngine, "mapTextureObjects", "field_110585_a");
                
                Minecraft.getMinecraft().renderEngine.deleteTexture(mapTexture);
                Minecraft.getMinecraft().renderEngine.deleteTexture(sapiaMapTexture);
                ITextureObject texture1 = textureMap.remove(mapTexture);
                if(texture1 != null && texture1 instanceof DynamicTexture) {
                    ReflectionUtils.Fields.setPrivateValue(DynamicTexture.class, (DynamicTexture) texture1, new int[0], "dynamicTextureData", "field_110566_b");
                }
                ITextureObject texture2 = textureMap.remove(sapiaMapTexture);
                if(texture2 != null && texture2 instanceof DynamicTexture) {
                    ReflectionUtils.Fields.setPrivateValue(DynamicTexture.class, (DynamicTexture) texture2, new int[0], "dynamicTextureData", "field_110566_b");
                }
                mapTexture = null;
                sapiaMapTexture = null;
                clearThread();
            }
        }
        
        @SideOnly(value = Side.CLIENT)
        private void reloadClientMap() {
            if(clientMapGenThread != null && !clientMapGenThread.isDone()) {
                clientMapGenThread.interrupt();
                Ainulindale.log.info("Halting map gen thread to start a new one. (" + id + ")");
                try {
                    Thread.sleep(5); //Just to make sure that the old thread had time to stop
                }
                catch(InterruptedException e) {
                    e.printStackTrace();
                }
                clearThread();
            }
            
            clientMapGenThread = new ClientMapThread();
        }

        @SideOnly(value = Side.CLIENT)
        public boolean needsReload() {
            return mapTexture == null || sapiaMapTexture == null;
        }

        @SideOnly(value = Side.CLIENT)
        public void tickThread() {
            if(clientMapGenThread != null && clientMapGenThread.isDone()) {
                setClientMaps(clientMapGenThread.resultMap, clientMapGenThread.resultSapia);
                clearThread();
            }
        }

        @SideOnly(value = Side.CLIENT)
        public boolean isThreadRunning() {
            return clientMapGenThread != null && !clientMapGenThread.isDone();
        }

        @SideOnly(value = Side.CLIENT)
        private void clearThread() {
            if(clientMapGenThread != null) {
                clientMapGenThread.resultMap = null;
                clientMapGenThread.resultSapia = null;
                clientMapGenThread = null;
            }
        }

        @SideOnly(value = Side.CLIENT)
        private BufferedImage generateClientMaps() {
            BufferedImage map = null;
            try {
                map = generateClientMap();
            }
            catch(IOException e) {
                Ainulindale.log.error("Failed to load client map for " + id);
                e.printStackTrace();
            }
            
            //Hide biomes
            Map<Integer, Integer> colorRemap = new HashMap<Integer, Integer>();
            
            for(HiddenBiome biome : HiddenBiomeCacheClient.currentHiddenBiomes) {
                colorRemap.put(biome.getRealBiome().color, biome.getHiddenReplacementBiome().color);
            }
            
            if(!colorRemap.isEmpty()) {
                // Probably a slow way of doing it but it works.
                for(int x = 0; x < map.getWidth(); x++) {
                    for(int y = 0; y < map.getHeight(); y++) {
                        int color = map.getRGB(x, y);
                        Integer remapedColor = colorRemap.get(color);
                        
                        if(remapedColor != null) map.setRGB(x, y, remapedColor);
                    }
                }
            }
            
            
            return map;
        }

        @SideOnly(value = Side.CLIENT)
        private void setClientMaps(BufferedImage map, BufferedImage sapiaMap) {
            Map<ResourceLocation, ITextureObject> textureMap = ReflectionUtils.Fields.getPrivateValue(TextureManager.class, Minecraft.getMinecraft().renderEngine, "mapTextureObjects", "field_110585_a");
            
            if(mapTexture != null) {
                Minecraft.getMinecraft().renderEngine.deleteTexture(mapTexture);
                ITextureObject texture = textureMap.remove(mapTexture);
                if(texture != null && texture instanceof DynamicTexture) {
                    ReflectionUtils.Fields.setPrivateValue(DynamicTexture.class, (DynamicTexture) texture, new int[0], "dynamicTextureData", "field_110566_b");
                }
            }
            mapTexture = Minecraft.getMinecraft().renderEngine.getDynamicTextureLocation(id.getResourcePath(), new DynamicTexture(map));
            
            if(sapiaMapTexture != null) {
                Minecraft.getMinecraft().renderEngine.deleteTexture(sapiaMapTexture);
                ITextureObject texture = textureMap.remove(sapiaMapTexture);
                if(texture != null && texture instanceof DynamicTexture) {
                    ReflectionUtils.Fields.setPrivateValue(DynamicTexture.class, (DynamicTexture) texture, new int[0], "dynamicTextureData", "field_110566_b");
                }
            }
            sapiaMapTexture = Minecraft.getMinecraft().renderEngine.getDynamicTextureLocation(id.getResourcePath() + "_sapia", new DynamicTexture(sapiaMap));

            LOTRReflectionHelpers.LOTRMap.setClientSideMapTexture(mapTexture, sapiaMapTexture);
        }

        @SideOnly(value = Side.CLIENT)
        private class ClientMapThread extends Thread {
            public volatile BufferedImage resultMap = null;
            public volatile BufferedImage resultSapia = null;
            
            public ClientMapThread() {
                super("ClientMapGeneratoinThread");
                this.setDaemon(true);
                this.start();
            }
            
            public boolean isDone() {
                return resultMap != null && resultSapia != null;
            }
            
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                resultMap = generateClientMaps();
                resultSapia = TextureUtils.convertToSepia(resultMap);
                Ainulindale.log.info("Took " + (System.currentTimeMillis() - startTime) + " ms to generate the client map (" + id + ")");
            }
        }
    }
    
    /**
     * A simple implementation of the {@link MapInfo} which will use a resource location pointing in a mods assets as the main map.
     */
    public static class SimpleMapInfo extends MapInfo {
        protected ResourceLocation mapResource;
        protected ResourceLocation iconResource;
        
        
        /**
         * Creates a new <code>SimpleMapInfo</code> instance from the given {@link ResourceLocation}.
         * <p>
         * The {@link MapInfo#id} will be the provided {@link ResourceLocation}.
         * 
         * @param res the map resource location
         */
        public SimpleMapInfo(ResourceLocation res) {
            super(res);
            mapResource = res;
            iconResource = new ResourceLocation(res.getResourceDomain(), res.getResourcePath().substring(0, res.getResourcePath().length() - 4) + "_icon.png");
        }
        
        @Override
        @SideOnly(value = Side.CLIENT)
        protected BufferedImage generateClientMap() throws IOException {
            return InputStreamUtils.getImage(Minecraft.getMinecraft().getResourceManager().getResource(mapResource).getInputStream());
        }
        
        @Override
        protected BufferedImage generateServerMap() throws IOException {
            return InputStreamUtils.getImage(ResourceHelper.getInputStream(mapResource));
        }

        @Override
        public ResourceLocation getIcon() {
            return iconResource;
        }
    }
    
    /**
     * A implementation of the {@link MapInfo} which will use a file. This file should be a external file which isn't part of the mod assets.
     */
    public static class FileMapInfo extends MapInfo {
        private static ResourceLocation icon = new ResourceLocation(Ainulindale.MODID, "config/icon.png");
        protected File imageFile;
        
        
        /**
         * Creates a new <code>FileMapInfo</code> instance from the given {@link File}.
         * <p>
         * The {@link MapInfo#id} will be base on the relativized path from the main minecraft folder.
         * 
         * @param file the map file.
         */
        public FileMapInfo(File file) {
            super(new ResourceLocation("file", Ainulindale.mainFolderPath.toPath().relativize(file.toPath()).toString()));
            imageFile = file;
        }
        
        @Override
        @SideOnly(value = Side.CLIENT)
        protected BufferedImage generateClientMap() throws IOException {
            return InputStreamUtils.getImage(new FileInputStream(imageFile));
        }
        
        @Override
        protected BufferedImage generateServerMap() throws IOException {
            return InputStreamUtils.getImage(new FileInputStream(imageFile));
        }

        @Override
        public ResourceLocation getIcon() {
            return icon;
        }
    }
    
    public static class OverlayMapInfo extends SimpleMapInfo {
        private ResourceLocation overlayRes;

        public OverlayMapInfo(ResourceLocation mainMapRes, ResourceLocation overlayRes) {
            super(mainMapRes);
            this.overlayRes = overlayRes;
        }
       
        @Override
        @SideOnly(value = Side.CLIENT)
        protected BufferedImage generateClientMap() throws IOException {
            BufferedImage mainMap = InputStreamUtils.getImage(Minecraft.getMinecraft().getResourceManager().getResource(mapResource).getInputStream());
            BufferedImage overlayMap = InputStreamUtils.getImage(Minecraft.getMinecraft().getResourceManager().getResource(overlayRes).getInputStream());
            
            if(mainMap.getWidth() != overlayMap.getWidth() || mainMap.getHeight() != overlayMap.getHeight()) {
                Ainulindale.log.error("Failed to load client overlay map for " + getID() + " as both maps aren't the same size.");
                return null;
            }
            
            // I know this is probably a slow method of doing it but rasters and graphics confuse me
            for(int x = 0; x < overlayMap.getWidth(); x++) {
                for(int y = 0; y < overlayMap.getHeight(); y++) {
                    int color = overlayMap.getRGB(x, y);
                    
                    if(color >> 24 != 0) mainMap.setRGB(x, y, color);
                }
            }

            return mainMap;
        }
        
    }
}
