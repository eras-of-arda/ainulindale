package eoa.ainulindale.api.registries;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.reflection.LOTREnumHelpers;
import eoa.ainulindale.common.util.resources.ShieldProperties;
import lotr.common.LOTRShields;
import lotr.common.fac.LOTRFaction;

import java.util.*;

/**
 * The shield registry.
 * <p>
 * Use this class to register and or get any {@link LOTRShields}. When adding or removing a shield you should always use this class instead of directly using {@link LOTRShields}.
 */
public class ShieldRegistry {
    static Map<String, List<LOTRShields>> shields = new HashMap<String, List<LOTRShields>>();
    static Map<LOTRShields, Boolean> shieldHiddenStatus = new HashMap<LOTRShields, Boolean>();
    static Map<LOTRShields, UUID[]> shieldUUIDList = new HashMap<LOTRShields, UUID[]>();
    static Map<LOTRShields, LOTRShields.ShieldType> shieldTypeList = new HashMap<LOTRShields, LOTRShields.ShieldType>();

    /**
     * Creates an alignment shield for a given {@link LOTRFaction} and addon ID
     *
     * @param addonID the addon ID
     * @param name the shield name
     * @param fac the alignment faction the shield is linked to
     * @return the new shield
     */
    public static LOTRShields createAlignmentShield(String addonID, String name, LOTRFaction fac)
    {

        String namespacedName = "";

        if(addonID == "lotr")
        {
            namespacedName = name;
        }
        else
        {
            namespacedName = addonID+"$"+name;
        }

        LOTRShields shield = LOTREnumHelpers.addAlignmentShield(namespacedName, fac);
        if(!shields.containsKey(addonID)) shields.put(addonID, new ArrayList<LOTRShields>());
        shields.get(addonID).add(shield);

        shieldHiddenStatus.put(shield, false);
        shieldTypeList.put(shield, shield.shieldType);
        shieldUUIDList.put(shield, ReflectionHelper.getPrivateValue(LOTRShields.class, shield, "exclusiveUUIDs"));
        return shield;
    }

    /*public static LOTRShields createAchievementShield(String addonID, String name)
    {

    }*/ //does not work yet because I'll need to coremod the shield code to make it work


    /**
     * Creates an exclusive shield for a given addon ID and list of player UUIDs
     *
     * @param addonID the addon ID
     * @param name the shield name
     * @param hidden is the shield visible in the menu to those who do not have it unlocked
     * @param UUIDs the UUIDs of players who have the shield
     * @return the new shield
     */
    public static LOTRShields createExclusiveShield(String addonID, String name, boolean hidden, String... UUIDs)
    {

        String namespacedName = "";

        if(addonID == "lotr")
        {
            namespacedName = name;
        }
        else
        {
            namespacedName = addonID+"$"+name;
        }

        LOTRShields shield = LOTREnumHelpers.addShield(namespacedName, hidden, UUIDs);
        if(!shields.containsKey(addonID)) shields.put(addonID, new ArrayList<LOTRShields>());
        shields.get(addonID).add(shield);

        shieldHiddenStatus.put(shield, hidden);
        shieldTypeList.put(shield, shield.shieldType);
        shieldUUIDList.put(shield, ReflectionHelper.getPrivateValue(LOTRShields.class, shield, "exclusiveUUIDs"));
        return shield;
    }

    /**
     * Registers an existing shield for a given addon ID
     *
     * @param addonID the addon ID
     * @param shield the shield to register
     * @return the shield just registered
     */
    public static LOTRShields registerShield(String addonID, LOTRShields shield)
    {
        if(!shields.containsKey(addonID)) shields.put(addonID, new ArrayList<LOTRShields>());
        shields.get(addonID).add(shield);

        shieldHiddenStatus.put(shield, ReflectionHelper.getPrivateValue(LOTRShields.class, shield, "isHidden"));
        shieldTypeList.put(shield, shield.shieldType);
        shieldUUIDList.put(shield, ReflectionHelper.getPrivateValue(LOTRShields.class, shield, "exclusiveUUIDs"));

        return shield;
    }

    static void hideShield(LOTRShields shield)
    {
        ReflectionHelper.setPrivateValue(LOTRShields.class, shield, true, "isHidden");
        shield.shieldType = LOTRShields.ShieldType.EXCLUSIVE;
        ReflectionHelper.setPrivateValue(LOTRShields.class, shield, new UUID[] {}, "exclusiveUUIDs");

    }

    static void restoreShield(LOTRShields shield)
    {
        ReflectionHelper.setPrivateValue(LOTRShields.class, shield, shieldHiddenStatus.get(shield), "isHidden");
        shield.shieldType = shieldTypeList.get(shield);
        ReflectionHelper.setPrivateValue(LOTRShields.class, shield, shieldUUIDList.get(shield), "exclusiveUUIDs");
    }

    /**
     * Gets a shield by name for a given addon ID
     *
     * @param addonID the addon ID
     * @param name the name of the shield to get
     * @return the {@link LOTRShields} object for this shield, or null if it does not exist
     */
    public static LOTRShields getShield(String addonID, String name)
    {

        String namespacedName = "";

        if(addonID == "lotr")
        {
            namespacedName = name;
        }
        else
        {
            namespacedName = addonID+"$"+name;
        }

        if(!shields.containsKey(addonID)) shields.put(addonID, new ArrayList<LOTRShields>());
        for(LOTRShields shield : shields.get(addonID))
        {
            if(shield.getShieldName() == namespacedName || shield.getShieldName() == name)
            {
                return shield;
            }
        }
        return null;
    }

    /**
     * Gets the list of shields for a given addon ID
     *
     * @param addonID the addon ID
     * @return the {@link List<LOTRShields>} object for this shield
     */
    public static List<LOTRShields> getShields(String addonID)
    {

        if(!shields.containsKey(addonID)) shields.put(addonID, new ArrayList<LOTRShields>());
        return shields.get(addonID);
    }


}
