package eoa.ainulindale.api.registries;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.util.resources.TitleProperties;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRShields;
import lotr.common.LOTRTitle;
import lotr.common.fac.LOTRFactionRank;
import lotr.common.world.map.LOTRMountains;

import java.util.*;

public class TitleRegistry {
    /**
     * The title registry.
     * <p>
     * Use this class to register and or get any {@link lotr.common.LOTRTitle}. When adding or removing a title you should always use this class instead of directly using {@link lotr.common.LOTRTitle}.
     */
    static Map<String, List<LOTRTitle>> titles = new HashMap<String, List<LOTRTitle>>();

    static Map<LOTRTitle, TitleProperties> titleProperties = new HashMap<LOTRTitle, TitleProperties>();

    static Map<String, Map<LOTRFactionRank, List<LOTRTitle>>> titlesForRank = new HashMap<String, Map<LOTRFactionRank, List<LOTRTitle>>>();

    /**
     * Creates a new {@link LOTRTitle} instance for a starter (always unlocked) title for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this title
     * @param name the name of the new title you are creating
     * @return the newly created {@link LOTRTitle}
     */
    public static LOTRTitle addStarterTitle(String addonID, String name)
    {
        String namespacedName = "";

        if(addonID == "lotr")
        {
            namespacedName = name;
        }
        else
        {
            namespacedName = addonID+"$"+name;
        }
        LOTRTitle title = new LOTRTitle(namespacedName);
        if(!titles.containsKey(addonID)) titles.put(addonID, new ArrayList<LOTRTitle>());
        titles.get(addonID).add(title);
        TitleProperties props = new TitleProperties();
        props.titleType = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
        props.isHidden = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "isHidden");
        props.uuids = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "uuids");
        titleProperties.put(title, props);
        return title;

    }

    /**
     * Creates a new {@link LOTRTitle} instance for an achievement-linked title for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this title
     * @param name the name of the new title you are creating
     * @param achievement the name of the achievement the title is linked to
     * @return the newly created {@link LOTRTitle}
     */
    public static LOTRTitle addAchievementTitle(String addonID, String name, LOTRAchievement achievement)
    {
        String namespacedName = "";

        if(addonID == "lotr")
        {
            namespacedName = name;
        }
        else
        {
            namespacedName = addonID+"$"+name;
        }
        LOTRTitle title = new LOTRTitle(namespacedName, achievement);
        if(!titles.containsKey(addonID)) titles.put(addonID, new ArrayList<LOTRTitle>());
        titles.get(addonID).add(title);
        TitleProperties props = new TitleProperties();
        props.titleType = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
        props.isHidden = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "isHidden");
        props.uuids = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "uuids");
        titleProperties.put(title, props);
        return title;

    }

    /**
     * Creates a new {@link LOTRTitle} instance for an faction rank title for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this title
     * @param name the name of the new title you are creating
     * @param feminine whether this is the feminine version of a gendered title
     * @return the newly created {@link LOTRTitle}
     */
    public static LOTRTitle addRankTitle(String addonID, String name, LOTRFactionRank rank, boolean feminine)
    {
        LOTRTitle title = new LOTRTitle(rank, feminine);
        if(!titles.containsKey(addonID)) titles.put(addonID, new ArrayList<LOTRTitle>());
        titles.get(addonID).add(title);
        TitleProperties props = new TitleProperties();
        props.titleType = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
        props.isHidden = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "isHidden");
        props.uuids = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "uuids");
        titleProperties.put(title, props);

        if (!titlesForRank.containsKey(addonID)) titlesForRank.put(addonID, new HashMap<LOTRFactionRank, List<LOTRTitle>>());
        if (!titlesForRank.get(addonID).containsKey(rank)) titlesForRank.get(addonID).put(rank, new ArrayList<LOTRTitle>());
        titlesForRank.get(addonID).get(rank).add(title);

        return title;

    }

    /**
     * Creates a new {@link LOTRTitle} instance for an exclusive title for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this title
     * @param name the name of the new title you are creating
     * @param UUIDs the UUIDs of the players who should have this title
     * @return the newly created {@link LOTRTitle}
     */
    public static LOTRTitle addExclusiveTitle(String addonID, String name, String... UUIDs)
    {
        String namespacedName = "";

        if(addonID == "lotr")
        {
            namespacedName = name;
        }
        else
        {
            namespacedName = addonID+"$"+name;
        }
        LOTRTitle title = new LOTRTitle(namespacedName);
        title.setPlayerExclusive(UUIDs);
        if(!titles.containsKey(addonID)) titles.put(addonID, new ArrayList<LOTRTitle>());
        titles.get(addonID).add(title);
        TitleProperties props = new TitleProperties();
        props.titleType = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
        props.isHidden = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "isHidden");
        props.uuids = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "uuids");
        titleProperties.put(title, props);
        return title;
    }

    /**
     * Registers an existing {@link LOTRTitle} for the given addon ID
     *
     * @param addonID the addon ID for which you want to register this title
     * @param title the {@link LOTRTitle} you are registering
     * @return the newly registered {@link LOTRTitle}
     */
    public static LOTRTitle registerTitle(String addonID, LOTRTitle title)
    {
        if(!titles.containsKey(addonID)) titles.put(addonID, new ArrayList<LOTRTitle>());
        titles.get(addonID).add(title);
        TitleProperties props = new TitleProperties();
        props.titleType = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
        props.isHidden = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "isHidden");
        props.uuids = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "uuids");
        titleProperties.put(title, props);

        if(props.titleType == LOTRTitle.TitleType.RANK)
        {
            LOTRFactionRank rank = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleRank");
            if (!titlesForRank.containsKey(addonID)) titlesForRank.put(addonID, new HashMap<LOTRFactionRank, List<LOTRTitle>>());
            if (!titlesForRank.get(addonID).containsKey(rank)) titlesForRank.get(addonID).put(rank, new ArrayList<LOTRTitle>());
            titlesForRank.get(addonID).get(rank).add(title);
        }

        return title;
    }

    /**
     * Gets the list of {@link LOTRTitle} instances for the given addon ID
     *
     * @param addonID the addon ID for which you want to get titles
     * @return a {@link List} of the {@link LOTRTitle} instances for this addon
     */
    public static List<LOTRTitle> getTitles(String addonID)
    {
        if(!titles.containsKey(addonID)) titles.put(addonID, new ArrayList<LOTRTitle>());
        return titles.get(addonID);
    }

    /**
     * Gets the list of {@link LOTRTitle} instances for the given addon ID and rank
     *
     * @param addonID the addon ID for which you want to get rank titles
     * @param rank the rank for which you want to get titles
     * @return a {@link List} of the {@link LOTRTitle} instances for this addon and rank
     */
    public static List<LOTRTitle> getTitlesForRank(String addonID, LOTRFactionRank rank)
    {
        if (!titlesForRank.containsKey(addonID)) titlesForRank.put(addonID, new HashMap<LOTRFactionRank, List<LOTRTitle>>());
        if (!titlesForRank.get(addonID).containsKey(rank)) titlesForRank.get(addonID).put(rank, new ArrayList<LOTRTitle>());
        return titlesForRank.get(addonID).get(rank);
    }

    /**
     * Hides the given {@link LOTRTitle} instance
     *
     * @param title the title you want to hide
     */
    static void hideTitle(LOTRTitle title)
    {
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, LOTRTitle.TitleType.PLAYER_EXCLUSIVE,"titleType");
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, true, "isHidden");
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, new UUID[]{}, "uuids");
    }

    /**
     * Shows the given {@link LOTRTitle} instance
     *
     * @param title the title you want to show
     */
    static void showTitle(LOTRTitle title)
    {
        TitleProperties props = titleProperties.get(title);
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, props.titleType,"titleType");
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, props.isHidden, "isHidden");
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, props.uuids, "uuids");
    }

}
