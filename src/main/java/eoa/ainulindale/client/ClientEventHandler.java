package eoa.ainulindale.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import eoa.ainulindale.client.gui.GuiMapList;
import eoa.ainulindale.client.gui.button.GuiButtonAction;
import eoa.ainulindale.client.gui.button.GuiButtonOpenGui;
import eoa.ainulindale.common.CommonEventHandler;
import lotr.client.gui.LOTRGuiMainMenu;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraftforge.client.event.GuiScreenEvent;

public class ClientEventHandler extends CommonEventHandler {
    
    @SubscribeEvent
    public void postInitGui(GuiScreenEvent.InitGuiEvent.Post event) {
        if(event.gui instanceof LOTRGuiMainMenu) {
            GuiButton buttonLanguage = null;
            GuiButton buttonMods = null;
            
            for(Object obj : event.buttonList) {
                GuiButton button = (GuiButton) obj;
                
                if(button.id == 6) buttonMods = button;
                else if(button.id == 5) buttonLanguage = button;
            }
            
            
            GuiButtonAction mapListButton = new GuiButtonOpenGui(333, buttonLanguage.xPosition, buttonMods.yPosition, 20, 20, null, Minecraft.getMinecraft(), GuiMapList.class).setRedBookRender(true);
            event.buttonList.add(mapListButton);
        }
    }

    @SubscribeEvent
    public void postActionPerformed(GuiScreenEvent.ActionPerformedEvent.Post event) {
        if(event.gui instanceof LOTRGuiMainMenu && event.button instanceof GuiButtonOpenGui) {
            ((GuiButtonOpenGui) event.button).openGui();
        }
    }

    @SubscribeEvent
    public void joinServerEvent(FMLNetworkEvent.ClientConnectedToServerEvent event) {
        if(!event.isLocal) ClientTickHandler.mapTypePacketTimeout = 5 * 20;
    }

    @SubscribeEvent
    public void joinServerEvent(FMLNetworkEvent.ClientDisconnectionFromServerEvent event) {
        ClientTickHandler.mapTypePacketTimeout = -1;
    }
}
