package eoa.ainulindale.client;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import eoa.ainulindale.common.CommonProxy;

public class ClientProxy extends CommonProxy {

    @Override
    public void init(FMLInitializationEvent event) {
        super.init(event);
        HiddenBiomeCacheClient.initHiddenBiomes();
    }
    
    @Override
    public void registerSidedHandlers() {
        tickHandler = new ClientTickHandler();
        eventHandler = new ClientEventHandler();
    }
}
