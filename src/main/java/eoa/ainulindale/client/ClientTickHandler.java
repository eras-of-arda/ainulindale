package eoa.ainulindale.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.common.CommonTickHandler;
import lotr.common.LOTRModInfo;
import net.minecraft.util.ResourceLocation;

public class ClientTickHandler extends CommonTickHandler {
    public static int mapTypePacketTimeout = -1;
    
    @SubscribeEvent
    public void clientTickEvent(ClientTickEvent event) {
        if(event.phase == Phase.START) {
            MapRegistry.getActiveMap().tickThread();
        }
        else {
            // Now the best way to do this but apparently the client doesn't store the server mods.
            if(mapTypePacketTimeout >= 0) {
                if(mapTypePacketTimeout == 0) {
                    ResourceLocation defaultID = new ResourceLocation(LOTRModInfo.modID, "map/map.png");
                    if(!MapRegistry.getActiveMap().getID().equals(defaultID)) MapRegistry.loadMap(defaultID); //TODO: broken
                    if(MapRegistry.getActiveMap().needsReload()) MapRegistry.reloadActiveMap();
                }
                mapTypePacketTimeout--;
            }
        }
    }
}
