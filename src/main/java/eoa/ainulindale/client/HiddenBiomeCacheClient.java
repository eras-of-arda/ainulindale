package eoa.ainulindale.client;

import java.util.ArrayList;
import java.util.List;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.common.world.biome.HiddenBiome;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.LOTRLevelData;
import lotr.common.world.biome.LOTRBiome;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

public class HiddenBiomeCacheClient {
    private static List<HiddenBiome> allHiddenBiomes = new ArrayList<HiddenBiome>();
    public static List<HiddenBiome> currentHiddenBiomes = new ArrayList<HiddenBiome>();
    
    public static void initHiddenBiomes() {
        for(LOTRBiome biome : LOTRDimension.MIDDLE_EARTH.biomeList) {
            if(biome != null && biome instanceof HiddenBiome) allHiddenBiomes.add((HiddenBiome) biome);
        }
        
        updateAllCurrentHiddenBiomes(true);
    }
    
    public static void updateAllCurrentHiddenBiomes(boolean startup) {
        boolean changed = false;
        
        for(HiddenBiome biome : allHiddenBiomes) {
            EntityPlayer player = Minecraft.getMinecraft().thePlayer;
            
            if(!currentHiddenBiomes.contains(biome) && (player == null || !LOTRLevelData.getData(player).hasAchievement(biome.getRealBiome().getBiomeAchievement()))) {
                currentHiddenBiomes.add(biome);
                changed = true;
            }
            else if(player != null && LOTRLevelData.getData(player).hasAchievement(biome.getRealBiome().getBiomeAchievement())) {
                currentHiddenBiomes.remove(biome);
                changed = true;
            }
        }
        
        if(changed && !startup) {
            MapRegistry.reloadActiveMap();
        }
    }
    
    public static void earnedBiomeAchievement(LOTRAchievement ach) {
        for(HiddenBiome biome : currentHiddenBiomes) {
            if(biome.getRealBiome().getBiomeAchievement() == ach) {
                currentHiddenBiomes.remove(biome);
                MapRegistry.reloadActiveMap();
                break;
            }
        }
    }
}
