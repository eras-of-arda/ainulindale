package eoa.ainulindale.client;

import java.awt.Color;
import java.awt.image.BufferedImage;
import lotr.client.LOTRTextures;
import lotr.common.LOTRConfig;
import lotr.common.LOTRDimension;
import lotr.common.util.LOTRColorUtil;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRBiomeGenMordor;

public class TextureUtils {
    
    public static BufferedImage convertToSepia(BufferedImage srcImage) {
        int mapWidth = srcImage.getWidth();
        int mapHeight = srcImage.getHeight();
        int[] colors = srcImage.getRGB(0, 0, mapWidth, mapHeight, null, 0, mapWidth);
        for(int i = 0; i < colors.length; ++i) {
            int color = colors[i];
            if(LOTRConfig.osrsMap) {
                Integer biomeID = LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.get(color);
                if(biomeID == null) {
                    color = LOTRTextures.getMapOceanColor(true);
                }
                else {
                    LOTRBiome biome = LOTRDimension.MIDDLE_EARTH.biomeList[biomeID];
                    color = biome.heightBaseParameter < 0.0f ? 6453158 : (biome.heightBaseParameter > 0.8f ? 14736861 : (biome.heightBaseParameter > 0.4f ? 6575407 : (biome instanceof LOTRBiomeGenMordor ? 3290677 : (biome.decorator.treesPerChunk > 1 ? 2775058 : (biome.temperature < 0.3f ? (biome.temperature < 0.2f ? 14215139 : 9470587) : (biome.rainfall < 0.2f ? 13548147 : 5468426))))));
                }
            }
            else {
                color = getSepia(color);
            }
            colors[i] = color;
        }
        BufferedImage newMapImage = new BufferedImage(mapWidth, mapHeight, 2);
        newMapImage.setRGB(0, 0, mapWidth, mapHeight, colors, 0, mapWidth);
        if(LOTRConfig.osrsMap) {
            BufferedImage temp = newMapImage;
            newMapImage = new BufferedImage(mapWidth, mapHeight, 2);
            for(int i = 0; i < mapWidth; ++i) {
                for(int j = 0; j < mapHeight; ++j) {
                    int y1;
                    int x1;
                    int total;
                    int rgb1;
                    int y;
                    int x;
                    int range;
                    int rgb = temp.getRGB(i, j);
                    if(rgb == 5468426) {
                        range = 8;
                        int water = 0;
                        total = 0;
                        for(x = -range; x < range; ++x) {
                            for(y = -range; y < range; ++y) {
                                x1 = i + x;
                                y1 = y + j;
                                if(x1 < 0 || x1 >= mapWidth || y1 < 0 || y1 >= mapHeight) continue;
                                rgb1 = temp.getRGB(x1, y1);
                                if(rgb1 == 6453158) {
                                    ++water;
                                }
                                ++total;
                            }
                        }
                        if(water > 0) {
                            float ratio = (float) water / (float) total;
                            rgb = LOTRColorUtil.lerpColors_I(5468426, 9279778, ratio * 2.0f);
                        }
                    }
                    else if(rgb == 14736861) {
                        range = 8;
                        int edge = 0;
                        total = 0;
                        for(x = -range; x < range; ++x) {
                            for(y = -range; y < range; ++y) {
                                x1 = i + x;
                                y1 = y + j;
                                if(x1 < 0 || x1 >= mapWidth || y1 < 0 || y1 >= mapHeight) continue;
                                rgb1 = temp.getRGB(x1, y1);
                                if(rgb1 != 14736861) {
                                    ++edge;
                                }
                                ++total;
                            }
                        }
                        if(edge > 0) {
                            float ratio = (float) edge / (float) total;
                            rgb = LOTRColorUtil.lerpColors_I(14736861, 9005125, ratio * 1.5f);
                        }
                    }
                    newMapImage.setRGB(i, j, rgb | 0xFF000000);
                }
            }
        }
        
        return newMapImage;
    }

    private static int getSepia(int rgb) {
        Color color = new Color(rgb);
        int alpha = rgb >> 24 & 0xFF;
        float[] colors = color.getColorComponents(null);
        float r = colors[0];
        float g = colors[1];
        float b = colors[2];
        float newR = r * 0.79f + g * 0.39f + b * 0.26f;
        float newG = r * 0.52f + g * 0.35f + b * 0.19f;
        float newB = r * 0.35f + g * 0.26f + b * 0.15f;
        newR = Math.min(Math.max(0.0f, newR), 1.0f);
        newG = Math.min(Math.max(0.0f, newG), 1.0f);
        newB = Math.min(Math.max(0.0f, newB), 1.0f);
        int sepia = new Color(newR, newG, newB).getRGB();
        return sepia |= alpha << 24;
    }

}
