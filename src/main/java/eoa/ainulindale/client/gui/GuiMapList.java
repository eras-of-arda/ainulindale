package eoa.ainulindale.client.gui;

import java.util.ArrayList;
import java.util.List;

import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.api.registries.MapRegistry.MapInfo;
import eoa.ainulindale.common.Ainulindale;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.StatCollector;

public class GuiMapList extends GuiScreen {
    
    private GuiButton loadMapButton;
    
    private GuiMapList.MapList mapList;
    private List<String> maps;
    private int selectedElementIndex = -1;
    
    
    @Override
    public void initGui() {
        this.maps = new ArrayList<String>(AddonRegistry.getRegisteredAddons());
        this.mapList = new GuiMapList.MapList();
        this.mapList.registerScrollButtons(0, 1);
        
        this.buttonList.add(loadMapButton = new GuiButton(2, (this.width - 72) / 2, this.height - 64 + (64 - 20) / 3, 72, 20, StatCollector.translateToLocal("ainulindale.gui.maplist.loadMap")));
        loadMapButton.enabled = false;
    }
    
    @Override
    protected void actionPerformed(GuiButton button) {
        if(button == loadMapButton) loadSelectedMap();
        
        mapList.actionPerformed(button);
    }
    
    @Override
    public void drawScreen(int mouseX, int mouseY, float f) {
        this.mapList.drawScreen(mouseX, mouseY, f);
        
        String s = StatCollector.translateToLocal("ainulindale.gui.maplist.title");
        this.drawCenteredString(this.fontRendererObj, s, this.width / 2, 13, 0xFFFFFF);
        super.drawScreen(mouseX, mouseY, f);
    }
    
    public void loadSelectedMap() {
        loadMapButton.enabled = false;
        Ainulindale.log.info("ADDONLOAD 4");
        AddonRegistry.loadAddon(AddonRegistry.getAddonForMapID(AddonRegistry.getAddonProperties(this.maps.get(selectedElementIndex)).mapID.toString()));
        MapRegistry.reloadActiveMap();
    }
    
    private class MapList extends GuiSlot {

        public MapList() {
            super(GuiMapList.this.mc, GuiMapList.this.width, GuiMapList.this.height, 32, GuiMapList.this.height - 64, 36);
        }

        @Override
        protected int getSize() {
            return GuiMapList.this.maps.size();
        }

        @Override
        protected void elementClicked(int element, boolean doubleClick, int mouseX, int mouseY) {
            GuiMapList.this.selectedElementIndex = element;
            boolean isActive = GuiMapList.this.maps.get(element) == AddonRegistry.getActiveAddon();
            
            if(!isActive) {
                GuiMapList.this.loadMapButton.enabled = true;
            }
            else {
                GuiMapList.this.loadMapButton.enabled = false;
            }
            
            if(doubleClick && !isActive) GuiMapList.this.loadSelectedMap();
        }

        @Override
        protected boolean isSelected(int element) {
            return GuiMapList.this.selectedElementIndex == element;
        }

        @Override
        protected void drawBackground() {
            GuiMapList.this.drawDefaultBackground();
        }

        @Override
        protected void drawSlot(int element, int elementX, int elementY, int p_148126_4_, Tessellator tessellator, int mouseX, int mouseY) {
            FontRenderer fontRenderer = GuiMapList.this.fontRendererObj;
            MapInfo map = AddonRegistry.getAddonProperties(GuiMapList.this.maps.get(element)).mapInfo;
            boolean active = MapRegistry.getActiveMap() == map;
            
            GuiMapList.this.mc.getTextureManager().bindTexture(map.getIcon());
            Gui.func_146110_a(elementX, elementY, 0.0F, 0.0F, 32, 32, 32.0F, 32.0F); //x,y  , u1, v1, w, h, u2, v2?
            
            GuiMapList.this.drawString(fontRenderer, map.getDisplayName(), elementX + 32 + 3, elementY + 3, 0xFFFFFF);
            if(active) {
                String s = StatCollector.translateToLocal("ainulindale.gui.maplist.map.active");
                if(map.isThreadRunning()) s += " - " + StatCollector.translateToLocal("ainulindale.gui.maplist.map.loading");
                GuiMapList.this.drawString(fontRenderer, s, elementX + 32 + 3, elementY + 18 + 3, 0xFFFFFF);
            }
        }
        
    }
}
