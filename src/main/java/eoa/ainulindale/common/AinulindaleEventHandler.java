package eoa.ainulindale.common;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import eoa.ainulindale.api.AinulindaleAPI;
import eoa.ainulindale.api.event.registry.WaypointRegistryEvent;
import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.FactionRegistry;
import eoa.ainulindale.api.registries.WaypointRegistry;
import eoa.ainulindale.common.util.resources.WaypointProperties;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRPlayerData;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.map.LOTRWaypoint;
import net.minecraft.entity.player.EntityPlayer;

public class AinulindaleEventHandler {
    
    public AinulindaleEventHandler() {
        AinulindaleAPI.LOTR_EVENT_BUS.register(this);
    }
    
    @SubscribeEvent
    public void registerWaypoints(WaypointRegistryEvent event) {
        for(LOTRWaypoint wp : LOTRWaypoint.values())
        {
            WaypointRegistry.registerWaypointWithoutAdding("lotr", wp);
        }
        Ainulindale.worldConfig.waypointRegionConfig.processJson();
        Ainulindale.worldConfig.waypointConfig.processJson();
    }

}
