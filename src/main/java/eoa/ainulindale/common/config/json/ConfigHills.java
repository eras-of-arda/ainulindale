package eoa.ainulindale.common.config.json;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import eoa.ainulindale.api.registries.WaypointRegistry;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.config.json.ConfigHills.JSONHill;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.map.LOTRWaypoint;

import java.util.Map;
import java.util.Map.Entry;

public class ConfigHills extends JsonConfigBase<Map<String, JSONHill>> {
    public ConfigHills(WorldConfig config, String addonID) {
        super("hills", config, addonID);
    }
    
    @Override
    public void processJson(Map<String, JSONHill> map) {
        if(map != null) {
            int i = 0;
            
            for(Entry<String, JSONHill> entry : map.entrySet()) {
                if(entry.getKey().startsWith("example_")) continue;
                
                entry.getValue().register(NAME_SPACE, entry.getKey());
                i++;
            }
            
            if(i > 0) Ainulindale.log.info("Registered " + i + " custom hills for addon "+NAME_SPACE+".");
        }
    }
    
    public static class JSONHill {
        @SerializedName("faction")
        private String factionName;
        @SerializedName("region")
        private String regionName;
        private int x;
        private int y;
        private boolean hidden;
        
        public JSONHill(String factionName, String regionName, int x, int y) {
            this(factionName, regionName, x, y, false);
        }
        
        public JSONHill(String factionName, String regionName, int x, int y, boolean hidden) {
            this.factionName = factionName;
            this.regionName = regionName;
            this.x = x;
            this.y = y;
            this.hidden = hidden;
        }
        
        public void register(String addonID, String name) {
            LOTRWaypoint.Region region = LOTRWaypoint.regionForName(regionName);
            LOTRFaction faction = LOTRFaction.forName(factionName);
            
            if(region == null) {
                Ainulindale.log.error("Can't find region " + regionName + " for the waypoint " + name + " from the json config ");
                return;
            }
            if(faction == null) {
                Ainulindale.log.error("Can't find faction " + factionName + " for the waypoint " + name + " from the json config ");
                return;
            }
            
            WaypointRegistry.addWaypoint(addonID, name, region, faction, x, y, hidden);
        }
    }

    @Override
    public TypeToken<Map<String, JSONHill>> getJsonDeserializationType() {
        return new TypeToken<Map<String, JSONHill>>() {};
    }
}
