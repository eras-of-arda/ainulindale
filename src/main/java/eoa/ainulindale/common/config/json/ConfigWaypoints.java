package eoa.ainulindale.common.config.json;

import java.util.Map;
import java.util.Map.Entry;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import eoa.ainulindale.api.registries.WaypointRegistry;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.config.json.ConfigWaypoints.JSONWaypoint;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.map.LOTRWaypoint;

public class ConfigWaypoints extends JsonConfigBase<Map<String, JSONWaypoint>> {
    public ConfigWaypoints(WorldConfig config, String addonID) {
        super("waypoints", config, addonID);
    }
    
    @Override
    public void processJson(Map<String, JSONWaypoint> map) {
        if(map != null) {
            int i = 0;
            
            for(Entry<String, JSONWaypoint> entry : map.entrySet()) {
                if(entry.getKey().startsWith("example_")) continue;
                
                entry.getValue().register(NAME_SPACE, entry.getKey());
                i++;
            }
            
            if(i > 0) Ainulindale.log.info("Registered " + i + " custom waypoints for addon "+NAME_SPACE+".");
        }
    }
    
    public static class JSONWaypoint {
        @SerializedName("faction")
        private String factionName;
        @SerializedName("region")
        private String regionName;
        private int x;
        private int y;
        private boolean hidden;
        
        public JSONWaypoint(String factionName, String regionName, int x, int y) {
            this(factionName, regionName, x, y, false);
        }
        
        public JSONWaypoint(String factionName, String regionName, int x, int y, boolean hidden) {
            this.factionName = factionName;
            this.regionName = regionName;
            this.x = x;
            this.y = y;
            this.hidden = hidden;
        }
        
        public void register(String addonID, String name) {
            LOTRWaypoint.Region region = LOTRWaypoint.regionForName(regionName);
            LOTRFaction faction = LOTRFaction.forName(factionName);
            
            if(region == null) {
                Ainulindale.log.error("Can't find region " + regionName + " for the waypoint " + name + " from the json config ");
                return;
            }
            if(faction == null) {
                Ainulindale.log.error("Can't find faction " + factionName + " for the waypoint " + name + " from the json config ");
                return;
            }
            
            WaypointRegistry.addWaypoint(addonID, name, region, faction, x, y, hidden);
        }
    }

    @Override
    public TypeToken<Map<String, JSONWaypoint>> getJsonDeserializationType() {
        return new TypeToken<Map<String, JSONWaypoint>>() {};
    }
}
