package eoa.ainulindale.common.network.C2S;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.MapRegistry;
import io.netty.buffer.ByteBuf;

public class PacketRequestMapKick implements IMessage {
    
    public PacketRequestMapKick() {
    }


    @Override
    public void toBytes(ByteBuf data) {
    }

    @Override
    public void fromBytes(ByteBuf data) {
    }

    public static class Handler implements IMessageHandler<PacketRequestMapKick, IMessage> {
        @Override
        public IMessage onMessage(PacketRequestMapKick packet, MessageContext context) {
            context.getServerHandler().kickPlayerFromServer("The server is using addon with id: " + AddonRegistry.getActiveAddon() + " but you don't have said addon registered.");
            
            return null;
        }
    }
}
