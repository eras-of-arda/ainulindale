package eoa.ainulindale.common.network.S2C;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import eoa.ainulindale.client.HiddenBiomeCacheClient;
import io.netty.buffer.ByteBuf;

public class PacketReloadHiddenBiomes implements IMessage {
    
    public PacketReloadHiddenBiomes() {
    }


    @Override
    public void toBytes(ByteBuf data) {
        // No data to send
    }

    @Override
    public void fromBytes(ByteBuf data) {
        // No data to receive
    }

    public static class Handler implements IMessageHandler<PacketReloadHiddenBiomes, IMessage> {
        @Override
        public IMessage onMessage(PacketReloadHiddenBiomes packet, MessageContext context) {
            HiddenBiomeCacheClient.updateAllCurrentHiddenBiomes(false);
            return null;
        }
    }
}
