package eoa.ainulindale.common.network.S2C;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.client.ClientTickHandler;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.network.ByteBufHelper;
import eoa.ainulindale.common.network.C2S.PacketRequestMapKick;
import io.netty.buffer.ByteBuf;
import net.minecraft.util.ResourceLocation;

public class PacketSetMapType implements IMessage {
    private String id;
    
    public PacketSetMapType() {
    }
    
    public PacketSetMapType(String id) {
        this.id = id;
    }


    @Override
    public void toBytes(ByteBuf data) {
        ByteBufHelper.writeString(data, id);
    }

    @Override
    public void fromBytes(ByteBuf data) {
        this.id = ByteBufHelper.readString(data);
    }

    public static class Handler implements IMessageHandler<PacketSetMapType, IMessage> {
        @Override
        public IMessage onMessage(PacketSetMapType packet, MessageContext context) {
            ClientTickHandler.mapTypePacketTimeout = -1;
            
            if(!AddonRegistry.addonExistsWithName(packet.id)) {
                return new PacketRequestMapKick();
            }

            //ResourceLocation id = AddonRegistry.getMapIDForAddon(packet.id);
            
            if(!AddonRegistry.getActiveAddon().equals(packet.id))
            {
                Ainulindale.log.info("ADDONLOAD 2");
                AddonRegistry.loadAddon(packet.id);
            }
            if(MapRegistry.getActiveMap().needsReload()) MapRegistry.reloadActiveMap();
            
            return null;
        }
    }
}
