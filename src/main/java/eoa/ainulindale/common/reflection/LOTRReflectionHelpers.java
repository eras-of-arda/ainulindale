package eoa.ainulindale.common.reflection;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.ReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.util.resources.InputStreamUtils;
import eoa.ainulindale.common.util.resources.ResourceHelper;
import lotr.client.LOTRTextures;
import lotr.client.gui.LOTRGuiMainMenu;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRAchievement.Category;
import lotr.common.LOTRCreativeTabs;
import lotr.common.LOTRDimension;
import lotr.common.LOTRFoods;
import lotr.common.LOTRLore;
import lotr.common.LOTRLore.LoreCategory;
import lotr.common.LOTRReflection;
import lotr.common.LOTRShields;
import lotr.common.LOTRTitle;
import lotr.common.LOTRTitle.TitleType;
import lotr.common.entity.item.LOTREntityBanner;
import lotr.common.entity.item.LOTREntityBannerWall;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTRNames;
import lotr.common.entity.npc.LOTRSpeech;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.fac.LOTRControlZone;
import lotr.common.fac.LOTRFaction;
import lotr.common.fac.LOTRFactionRank;
import lotr.common.fac.LOTRFactionRelations;
import lotr.common.fac.LOTRFactionRelations.FactionPair;
import lotr.common.fac.LOTRFactionRelations.Relation;
import lotr.common.item.LOTRItemArmor;
import lotr.common.item.LOTRItemBanner;
import lotr.common.item.LOTRItemBanner.BannerType;
import lotr.common.item.LOTRItemBow;
import lotr.common.item.LOTRItemMountArmor;
import lotr.common.item.LOTRItemMountArmor.Mount;
import lotr.common.item.LOTRItemMug.Vessel;
import lotr.common.item.LOTRMaterial;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuest.QuestFactoryBase;
import lotr.common.quest.LOTRMiniQuestFactory;
import lotr.common.util.LOTRVersionChecker;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.genlayer.LOTRGenLayerWorld;
import lotr.common.world.map.LOTRRoads;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRInvasions;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.spawning.LOTRSpawnList;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.scan.LOTRStructureScan;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class LOTRReflectionHelpers {
    
    /**
     * Map related reflection helpers.
     */
    public static class LOTRMap {
        /**
         * Sets the client side map textures, both arguments need to be valid {@link ResourceLocation}s.
         * 
         * @param mapTexture the resource location for the normal map texture
         * @param sapiaTexture the resource location for the sapia map texture
         */
        @SideOnly(value = Side.CLIENT)
        public static void setClientSideMapTexture(ResourceLocation mapTexture, ResourceLocation sapiaTexture) {
            ReflectionUtils.Fields.setPrivateValue(LOTRTextures.class, null, mapTexture, "mapTexture");
            ReflectionUtils.Fields.setPrivateValue(LOTRTextures.class, null, sapiaTexture, "sepiaMapTexture");
        }

        /**
         * Converts the given {@link BufferedImage} into a sapia version. After that it will be registered as a {@link DynamicTexture}. Returns the sapia map {@link ResourceLocation}.
         * 
         * @param map the original map
         * @param sapiaRes the sapia map base resource location
         * @return the new sapia map resource location
         */
        @SideOnly(value = Side.CLIENT)
        public static ResourceLocation convertToSapia(BufferedImage map, ResourceLocation sapiaRes) {
            return ReflectionUtils.Methods.invokeMethod(LOTRTextures.class, null, "convertToSepia", map, sapiaRes);
        }

        /**
         * Sets the {@link LOTRBiome} map data for {@link LOTRGenLayerWorld}.
         * <p>
         * The data is a byte array containing the biome id's. It's format should be like this:
         * <pre>data[y * imageWidth + x];</pre>
         * 
         * @param data the biome id data
         * @param width the map width in image pixels
         * @param height the map height in image pixels
         */
        public static void setBiomeData(byte[] data, int width, int height) {
            LOTRGenLayerWorld.imageWidth = width;
            LOTRGenLayerWorld.imageHeight = height;
            ReflectionUtils.Fields.setPrivateValue(LOTRGenLayerWorld.class, null, data, "biomeImageData");
        }
        
        public static LOTRBiome getLabelBiome(LOTRMapLabels label) {
            return ReflectionUtils.Fields.getPrivateValue(LOTRMapLabels.class, label, "biome");
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Returns the {@link LOTRCreativeTabs} with that given field name.
     * This shouldn't be used for {@link LOTRCreativeTabs#tabStory} use LOTRCreativeTabs.tabStory instead.
     * 
     * @param name Name
     * @return {@link LOTRCreativeTabs}
     */
    public static LOTRCreativeTabs getLOTRCreativeTab(String name) {
        return ReflectionHelper.getPrivateValue(LOTRCreativeTabs.class, null, name);
    }

    // Some biomes have the same names as vanilla ones and the same for as the creative tabs happen
    /**
     * Returns the {@link LOTRBiome} with that given field name.
     * This should only be used for biomes where you can't do LOTRBiome.biomeName.
     * 
     * @param name Name
     * @return {@link LOTRBiome}
     */
    public static LOTRBiome getLOTRBiome(String name) {
        return ReflectionHelper.getPrivateValue(LOTRBiome.class, null, name);
    }

    public static void disableWaypoint(LOTRWaypoint wp) {
        ReflectionHelper.setPrivateValue(LOTRWaypoint.class, wp, true, "isHidden");
        ReflectionHelper.setPrivateValue(LOTRWaypoint.class, wp, LOTRFaction.HOSTILE, "faction");
    }

    public static void addBannerToIDMap(BannerType banner) {
        HashMap<Integer, BannerType> bannerForIDMap = ReflectionHelper.getPrivateValue(LOTRItemBanner.BannerType.class, null, "bannerForID");
        bannerForIDMap.put(banner.bannerID, banner);
        ReflectionHelper.setPrivateValue(LOTRItemBanner.BannerType.class, null, bannerForIDMap, "bannerForID");
    }

//    public static List<Item> getAllLOTRFAItems() {
//        return getObjectFieldsOfType(LOTRFAItems.class, Item.class);
//    }

    public static void clearAllRelations() {
        ReflectionHelper.setPrivateValue(LOTRFactionRelations.class, null, new HashMap<FactionPair, Relation>(), "defaultMap");
    }

    public static void addControlZone(LOTRFaction faction, LOTRControlZone zone) {
        findAndInvokeMethod(zone, LOTRFaction.class, faction, "addControlZone", LOTRControlZone.class);
    }

    public static void clearControlZones(LOTRFaction faction) {
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, new ArrayList<LOTRControlZone>(), "controlZones");
    }

    public static void setFactionAchievementCategory(LOTRFaction faction, Category category) {
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, category, "achieveCategory");
    }

    public static LOTRFactionRank addFactionRank(LOTRFaction faction, float alignment, String name) {
        return addFactionRank(faction, alignment, name, false);
    }

    public static LOTRFactionRank addFactionRank(LOTRFaction faction, float alignment, String name, boolean gendered) {
        return findAndInvokeMethod(new Object[] {alignment, name, gendered}, LOTRFaction.class, faction, "addRank", float.class, String.class, boolean.class);
    }

//    public static List<LOTRFaction> getLOTRFAFactions() {
//        return getObjectFieldsOfType(LOTRFAFactions.class, LOTRFaction.class);
//    }

    public static void clearRoadDataBase() {
        Object dataBase = findAndInvokeConstructor("lotr.common.world.map.LOTRRoads$RoadPointDatabase");
        ReflectionHelper.setPrivateValue(LOTRRoads.class, null, dataBase, "roadPointDatabase");
    }

    public static void registerRoad(String name, Object... waypoints) {
        findAndInvokeMethod(new Object[] {name, waypoints}, LOTRRoads.class, null, "registerRoad", String.class, Object[].class);
    }

    public static void removeMapLabel(LOTRMapLabels label) {
        ReflectionHelper.setPrivateValue(LOTRMapLabels.class, label, 0.0001f, "maxZoom");
        ReflectionHelper.setPrivateValue(LOTRMapLabels.class, label, 0, "minZoom");
    }

    public static void changeInvasionIcon(LOTRInvasions invasion, Item icon) {
        ReflectionHelper.setPrivateValue(LOTRInvasions.class, invasion, icon, "invasionIcon");
    }

    public static void hideShield(LOTRShields shield) {
        ReflectionHelper.setPrivateValue(LOTRShields.class, shield, true, "isHidden");
    }

    public static void addSpeechBank(String name, boolean rand, List<String> lines) {
        Class<?> speechBankClass = LOTRSpeech.class.getDeclaredClasses()[0];
        Object speechBank = findAndInvokeConstructor(new Object[] {name, rand, lines}, speechBankClass, String.class, boolean.class, List.class);

        Map<String, Object> allSpeechBanks = ReflectionHelper.getPrivateValue(LOTRSpeech.class, null, "allSpeechBanks");
        allSpeechBanks.put(name, speechBank);
        ReflectionHelper.setPrivateValue(LOTRSpeech.class, null, allSpeechBanks, "allSpeechBanks");
    }

    public static void addNameBanks(Map<String, String[]> nameBanks) {
        Map<String, String[]> allNameBanks = ReflectionHelper.getPrivateValue(LOTRNames.class, null, "allNameBanks");
        allNameBanks.putAll(nameBanks);
        ReflectionHelper.setPrivateValue(LOTRNames.class, null, allNameBanks, "allNameBanks");
    }

    public static void addSTRScans(Map<String, LOTRStructureScan> scans) {
        Map<String, LOTRStructureScan> allScans = ReflectionHelper.getPrivateValue(LOTRStructureScan.class, null, "allLoadedScans");
        allScans.putAll(scans);
        ReflectionHelper.setPrivateValue(LOTRStructureScan.class, null, allScans, "allLoadedScans");
    }

    public static void disableLOTRUpdateChecker() {
        ReflectionHelper.setPrivateValue(LOTRVersionChecker.class, null, true, "checkedUpdate");
    }

    public static void setBannerWasEverProtecting(LOTREntityBanner banner, boolean bool) {
        ReflectionHelper.setPrivateValue(LOTREntityBanner.class, banner, bool, "wasEverProtecting");
    }

    public static boolean getBannerWasEverProtecting(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "wasEverProtecting");
    }

    public static void setBannerPlayerSpecificProtection(LOTREntityBanner banner, boolean bool) {
        ReflectionHelper.setPrivateValue(LOTREntityBanner.class, banner, bool, "playerSpecificProtection");
    }

    public static boolean getBannerPlayerSpecificProtection(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "playerSpecificProtection");
    }

    public static boolean getBannerSelfProtection(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "selfProtection");
    }

    public static boolean getBannerStructureProtection(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "structureProtection");
    }

    public static NBTTagCompound getBannerProtectData(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "protectData");
    }

    public static void setBannerProtectData(LOTREntityBanner banner, NBTTagCompound tag) {
        ReflectionHelper.setPrivateValue(LOTREntityBanner.class, banner, tag, "protectData");
    }

    public static NBTTagCompound getWallBannerProtectData(LOTREntityBannerWall banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBannerWall.class, banner, "protectData");
    }

    public static boolean getTitleUseAchievementName(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "useAchievementName");
    }

    public static LOTRAchievement getTitleTitleAchievement(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleAchievement");
    }

    public static TitleType getTitleTitleType(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
    }

    public static List<LOTRFaction> getTitleAlignmentFactions(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "alignmentFactions");
    }

    public static void removeTitles(List<LOTRTitle> titles) {
        for(LOTRTitle title : titles) removeTitle(title);
    }

    public static void removeTitle(LOTRTitle title) {
        if(getTitleTitleType(title) == TitleType.ACHIEVEMENT) {
            LOTRAchievement achievement = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleAchievement");
            ReflectionHelper.setPrivateValue(LOTRAchievement.class, achievement, null, "achievementTitle");
            ReflectionHelper.setPrivateValue(LOTRTitle.class, title, null, "titleAchievement");
        }

        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, true, "isHidden");
        LOTRTitle.allTitles.remove(title);
    }

    public static void addLoreToLoreCategory(LoreCategory category, LOTRLore lore) {
        List<LOTRLore> loreList = ReflectionHelper.getPrivateValue(LOTRLore.LoreCategory.class, category, "loreList");
        loreList.add(lore);
        ReflectionHelper.setPrivateValue(LOTRLore.LoreCategory.class, category, loreList, "loreList");
    }

    public static ToolMaterial getBowMaterial(LOTRItemBow bow) {
        return ReflectionHelper.getPrivateValue(LOTRItemBow.class, bow, "bowMaterial");
    }

    public static void setMaterialCraftingItem(LOTRMaterial material, Item item) {
        setMaterialCraftingItem(material, item, item);
    }

    public static void setMaterialCraftingItem(LOTRMaterial material, Item toolItem, Item armorItem) {
        findAndInvokeMethod(new Object[] {toolItem, armorItem}, LOTRMaterial.class, material, "setCraftingItems", Item.class, Item.class);
    }

    public static void addMiniQuest(LOTRMiniQuestFactory factory, QuestFactoryBase<? extends LOTRMiniQuest> questFactory) {
        findAndInvokeMethod(questFactory, LOTRMiniQuestFactory.class, factory, "addQuest", QuestFactoryBase.class);
    }

    public static void setHiringQuestFactory(LOTRMiniQuestFactory factory, int alignment) {
        Map<Class<? extends LOTRMiniQuest>, List<QuestFactoryBase<? extends LOTRMiniQuest>>> questFactoriesMap = ReflectionHelper.getPrivateValue(LOTRMiniQuestFactory.class, factory, "questFactories");
        for(List<QuestFactoryBase<? extends LOTRMiniQuest>> questFactories : questFactoriesMap.values()) {
            for(QuestFactoryBase<? extends LOTRMiniQuest> questFactory : questFactories) {
                questFactory.setRewardFactor(0.0f);
                questFactory.setHiring(alignment);
            }
        }
        ReflectionHelper.setPrivateValue(LOTRMiniQuestFactory.class, factory, questFactoriesMap, "questFactories");
    }

    public static void setMiniQuestFactoryAchievement(LOTRMiniQuestFactory factory, LOTRAchievement achievement) {
        findAndInvokeMethod(achievement, LOTRMiniQuestFactory.class, factory, "setAchievement", LOTRAchievement.class);
    }

    public static void setMiniQuestFactoryLore(LOTRMiniQuestFactory factory, LoreCategory... lore) {
        findAndInvokeMethod(new Object[] {lore}, LOTRMiniQuestFactory.class, factory, "setLore", LoreCategory[].class);
    }

    public static void setMiniQuestBaseSpeechGroup(LOTRMiniQuestFactory factory, LOTRMiniQuestFactory quest) {
        ReflectionHelper.setPrivateValue(LOTRMiniQuestFactory.class, factory, quest, "baseSpeechGroup");
    }

    public static String getArmorName(LOTRItemArmor armor) {
        return findAndInvokeMethod(LOTRItemArmor.class, armor, "getArmorName");
    }

    public static Mount getMountArmorType(LOTRItemMountArmor armor) {
        return ReflectionHelper.getPrivateValue(LOTRItemMountArmor.class, armor, "mountType");
    }

    public static void setTradeVessels(LOTRTradeEntries trade, LOTRFoods food) {
        setTradeVessels(trade, food.getDrinkVessels());
    }

    public static void setTradeVessels(LOTRTradeEntries trade, Vessel... vessels) {
        findAndInvokeMethod(new Object[] {vessels}, LOTRTradeEntries.class, trade, "setVessels", Vessel[].class);
    }

    public static void enableChestContentPouches(LOTRChestContents contents) {
        findAndInvokeMethod(LOTRChestContents.class, contents, "enablePouches");
    }

    public static void setChestContentLore(LOTRChestContents contents, int chance, LoreCategory... categories) {
        findAndInvokeMethod(new Object[] {chance, categories}, LOTRChestContents.class, contents, "setLore", int.class, LoreCategory[].class);
    }

    public static void setChestContentVessels(LOTRChestContents contents, LOTRFoods food) {
        setChestContentVessels(contents, food.getDrinkVessels());
    }

    public static void setChestContentVessels(LOTRChestContents contents, Vessel... vessels) {
        findAndInvokeMethod(new Object[] {vessels}, LOTRChestContents.class, contents, "setDrinkVessels", Vessel[].class);
    }

    public static void setLOTRNPCSize(LOTREntityNPC entity, float width, float height) {
        findAndInvokeMethod(new Object[] {width, height}, LOTREntityNPC.class, entity, new String[] {"setSize", "func_70105_a", "a"}, float.class, float.class);
    }

    public static void setToolMaterialRepairItem(ToolMaterial material, ItemStack item) {
        ReflectionHelper.setPrivateValue(ToolMaterial.class, material, item, "repairMaterial");
    }

    public static Object getTreeFactroy(Class<? extends Proxy> proxyClass, InvocationHandler handler) {
        return findAndInvokeConstructor(new Object[] {handler}, proxyClass, InvocationHandler.class);
    }

    public static LOTRSpawnList newLOTRSpawnList(LOTRSpawnEntry... entries) {
        return findAndInvokeConstructor(new Object[] {entries}, LOTRSpawnList.class, LOTRSpawnEntry[].class);
    }

    public static void setWorldGenMapImage(ResourceLocation res) {
        BufferedImage img = InputStreamUtils.getImage(ResourceHelper.getInputStream(res));
        LOTRGenLayerWorld.imageWidth = img.getWidth();
        LOTRGenLayerWorld.imageHeight = img.getHeight();

        int[] colors = img.getRGB(0, 0, LOTRGenLayerWorld.imageWidth, LOTRGenLayerWorld.imageHeight, null, 0, LOTRGenLayerWorld.imageWidth);
        byte[] biomeImageData = new byte[LOTRGenLayerWorld.imageWidth * LOTRGenLayerWorld.imageHeight];

        for(int i = 0; i < colors.length; ++i) {
            int color = colors[i];
            Integer biomeID = LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.get(color);
            if(biomeID != null) {
                biomeImageData[i] = (byte) biomeID.intValue();
                continue;
            }
            Ainulindale.log.error("Found unknown biome on map: " + Integer.toHexString(color) + " at location: " + (i % LOTRGenLayerWorld.imageWidth) + ", " + (i / LOTRGenLayerWorld.imageWidth));
            biomeImageData[i] = (byte) LOTRBiome.ocean.biomeID;
        }

        ReflectionHelper.setPrivateValue(LOTRGenLayerWorld.class, null, biomeImageData, "biomeImageData");
    }

    @SideOnly(value = Side.CLIENT)
    public static void setClientMapImage(ResourceLocation mapTexture) {
        ReflectionHelper.setPrivateValue(LOTRTextures.class, null, mapTexture, "mapTexture");

        ResourceLocation sepiaMapTexture;
        try {
            BufferedImage mapImage = InputStreamUtils.getImage(Minecraft.getMinecraft().getResourceManager().getResource(mapTexture).getInputStream());
            sepiaMapTexture = findAndInvokeMethod(new Object[] {mapImage, "lotr:map_sepia"}, LOTRTextures.class, null, "convertToSepia", BufferedImage.class, String.class);
        }
        catch(IOException e) {
            FMLLog.severe("Failed to generate LOTR sepia map", new Object[0]);
            e.printStackTrace();
            sepiaMapTexture = mapTexture;
        }

        ReflectionHelper.setPrivateValue(LOTRTextures.class, null, sepiaMapTexture, "sepiaMapTexture");
    }
    
    public static void setMainMenuWaypointRoute(List<LOTRWaypoint> waypointRoute) {
        ReflectionHelper.setPrivateValue(LOTRGuiMainMenu.class, null, waypointRoute, "waypointRoute");
    }
    
    public static <E, T> List<T> getObjectFieldsOfType(Class<? extends E> clazz, Class<? extends T> type) {
        return getObjectFieldsOfType(clazz, null, type);
    }

    @SuppressWarnings("unchecked")
    public static <E, T> List<T> getObjectFieldsOfType(Class<? extends E> clazz, E instance, Class<? extends T> type) {
        List<T> list = new ArrayList<T>();
        
        try {
            for (Field field : clazz.getDeclaredFields()) {
                if(field == null) continue;
                
                Object fieldObj = null;
                if(Modifier.isStatic(field.getModifiers())) fieldObj = field.get(null);
                else if(instance != null) fieldObj = field.get(instance);
                
                if(fieldObj == null) continue;
                
                if (type.isAssignableFrom(fieldObj.getClass())) {
                    list.add((T) fieldObj);
                }
            }
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            Ainulindale.log.error("Errored when getting all field from: " + clazz.getName() + " of type: " + type.getName());
        }

        return list;
    }
    
    public static <T, E> void setFinalField(Class<? super T> classToAccess, T instance, E value, String... fieldNames) {
        try {
            LOTRReflection.setFinalField(classToAccess, instance, value, fieldNames);
        }
        catch(Exception e) {
            Ainulindale.log.error("Error when setting field: " + fieldNames[0] + " for class: " + classToAccess.getName());
            e.printStackTrace();
        }
    }

    private static <T, E> T findAndInvokeMethod(Class<? super E> clazz, E instance, String methodName) {
        return findAndInvokeMethod(new Object[] {}, clazz, instance, methodName);
    }

    private static <T, E> T findAndInvokeMethod(Object arg, Class<? super E> clazz, E instance, String methodName, Class<?>... methodTypes) {
        return findAndInvokeMethod(new Object[] {arg}, clazz, instance, new String[] {methodName}, methodTypes);
    }

    private static <T, E> T findAndInvokeMethod(Object[] arg, Class<? super E> clazz, E instance, String methodName, Class<?>... methodTypes) {
        return findAndInvokeMethod(arg, clazz, instance, new String[] {methodName}, methodTypes);
    }

    @SuppressWarnings("unchecked")
    private static <T, E> T findAndInvokeMethod(Object[] args, Class<? super E> clazz, E instance, String[] methodNames, Class<?>... methodTypes) {
        Method addControlZoneMethod = ReflectionHelper.findMethod(clazz, instance, methodNames, methodTypes);
        try {
            return (T) addControlZoneMethod.invoke(instance, args);
        }
        catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            Ainulindale.log.error("Error when getting method " + methodNames[0] + " from class " + clazz.getSimpleName());
            e.printStackTrace();
        }

        return null;
    }

    private static <E> E findAndInvokeConstructor(String className, Class<?>... parameterTypes) {
        return findAndInvokeConstructor(new Object[] {}, className, parameterTypes);
    }

    private static <E> E findAndInvokeConstructor(Object[] args, String className, Class<?>... parameterTypes) {
        try {
            return findAndInvokeConstructor(args, (Class<? extends E>) Class.forName(className), parameterTypes);
        }
        catch(ClassNotFoundException e) {
            Ainulindale.log.error("Error when finding class " + className + " for a constructor.");
            e.printStackTrace();
        }
        return null;
    }
    
    private static <E> E findAndInvokeConstructor(Class<E> clazz, Object[] args) {
    	Class<?>[] paramaterTypes = new Class<?>[args.length];
    	for(int i = 0; i < args.length; i++) {
    		paramaterTypes[i] = args[i].getClass();
    	}
    	
    	return findAndInvokeConstructor(args, clazz, paramaterTypes);
    }

    private static <E> E findAndInvokeConstructor(Object[] args, Class<E> clazz, Class<?>... parameterTypes) {
        Constructor<E> constructor = findConstructor(clazz, parameterTypes);
        constructor.setAccessible(true);
        try {
            return constructor.newInstance(args);
        }
        catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            Ainulindale.log.error("Error when initializing constructor from class " + clazz.getSimpleName() + " with parameters " + args);
            e.printStackTrace();
        }
        return null;
    }

    private static <E> Constructor<E> findConstructor(Class<E> clazz, Class<?>... parameterTypes) {
        try {
            return clazz.getDeclaredConstructor(parameterTypes);
        }
        catch(NoSuchMethodException | SecurityException e) {
            Ainulindale.log.error("Error when getting constructor from class " + clazz.getSimpleName() + " with parameters " + parameterTypes);
            e.printStackTrace();
        }
        return null;
    }
}
