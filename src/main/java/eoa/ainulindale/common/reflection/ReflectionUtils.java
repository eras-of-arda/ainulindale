package eoa.ainulindale.common.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import cpw.mods.fml.relauncher.ReflectionHelper;
import cpw.mods.fml.relauncher.ReflectionHelper.UnableToAccessFieldException;
import cpw.mods.fml.relauncher.ReflectionHelper.UnableToFindMethodException;
import eoa.ainulindale.common.Ainulindale;

public class ReflectionUtils {
    public static Class<?>[] getClassArr(Object... args) {
        Class<?>[] classArr = new Class<?>[args.length];
        
        for(int i = 0; i < args.length; i++) {
            classArr[i] = getPrimitiveClass(args[i].getClass());
        }
        
        return classArr;
    }
    
    public static Class<?> getPrimitiveClass(Class<?> clazz) {
        if(clazz.equals(Boolean.class)) return Boolean.TYPE;
        if(clazz.equals(Integer.class)) return Integer.TYPE;
        if(clazz.equals(Float.class)) return Float.TYPE;
        if(clazz.equals(Double.class)) return Double.TYPE;
        if(clazz.equals(Byte.class)) return Byte.TYPE;
        if(clazz.equals(Short.class)) return Short.TYPE;
        if(clazz.equals(Character.class)) return Character.TYPE;
        
        return clazz;
    }
    
    public static void clearCaches() {
        Fields.cachedFields.clear();
        Methods.cachedMethods.clear();
        Constructors.cachedConstructors.clear();
    }
    
    public static class Fields {
        private static Map<Pair<Class<?>, String>, Field> cachedFields = new HashMap<Pair<Class<?>, String>, Field>();
        
        public static <T, E> T getPrivateValue(Class <? super E> classToAccess, E instance, String... fieldNames) {
            try {
                return (T) findField(classToAccess, fieldNames).get(instance);
            }
            catch (IllegalArgumentException | NullPointerException | IllegalAccessException e) {
                throw new UnableToAccessFieldException(fieldNames, e);
            }
        }

        public static <T, E> void setPrivateValue(Class <? super T > classToAccess, T instance, E value, String... fieldNames) {
            try {
                findField(classToAccess, fieldNames).set(instance, value);
            }
            catch (IllegalArgumentException | NullPointerException | IllegalAccessException e) {
                throw new UnableToAccessFieldException(fieldNames, e);
            }
        }
        
        public static Field findField(Class<?> clazz, String... fieldNames) {
            Field field = cachedFields.get(Pair.of(clazz, fieldNames[0]));
            
            if(field != null) return field;
            
            field = ReflectionHelper.findField(clazz, fieldNames);
            // Remove the final modifiers from the field if it's a final field
            if(Modifier.isFinal(field.getModifiers())) {
                setPrivateValue(Field.class, field, field.getModifiers() & ~Modifier.FINAL, "modifiers");
            }

            cachedFields.put(Pair.of(clazz, fieldNames[0]), field);

            return field;
        }
        
        public static <E> List<E> scrapeFields(Class<? extends E> clazz, E instance) {
            return scrapeFields(clazz, instance, clazz);
        }

        public static <E, T> List<T> scrapeFields(Class<? extends E> clazz, E instance, Class<? extends T> type) {
            List<T> list = new ArrayList<T>();
            
            try {
                for (Field field : clazz.getDeclaredFields()) {
                    if(field == null) continue;
                    
                    Object fieldObj = null;
                    if(Modifier.isStatic(field.getModifiers())) fieldObj = field.get(null);
                    else if(instance != null) fieldObj = field.get(instance);
                    
                    if(fieldObj == null) continue;
                    
                    if (type.isAssignableFrom(fieldObj.getClass())) {
                        list.add((T) fieldObj);
                    }
                }
            }
            catch (IllegalArgumentException | IllegalAccessException e) {
                Ainulindale.log.error("Errored when getting all field from: " + clazz.getName() + " of type: " + type.getName());
            }

            return list;
        }        
    }
    
    public static class Methods {
        private static Map<Pair<Class<?>, String>, Method> cachedMethods = new HashMap<Pair<Class<?>, String>, Method>();
        
        public static <T, E> T invokeMethod(Class<E> clazz, E instance, String methodName, Object... args) {
            return invokeMethod(clazz, instance, new String[] {methodName}, args);
        }
        
        public static <T, E> T invokeMethod(Class<E> clazz, E instance, String[] methodName, Object... args) {
            return invokeMethod(clazz, instance, methodName, args, ReflectionUtils.getClassArr(args));
        }

        public static <T, E> T invokeMethod(Class<E> clazz, E instance, String[] methodNames, Object[] args, Class<?>... argTypes) {
            Method method = findMethod(clazz, methodNames, argTypes);
            try {
                return (T) method.invoke(instance, args);
            }
            catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new Errors.UnableToInvokeMethodException(methodNames, e);
            }
        }
        
        public static Method findMethod(Class<?> clazz, String[] methodNames, Class<?>... argTypes) {
            Method method = cachedMethods.get(Pair.of(clazz, getMethodSignature(methodNames[0], argTypes)));
            
            if(method != null) return method;
            
            
            Exception failed = null;
            for (String methodName : methodNames) {
                try {
                    method = clazz.getDeclaredMethod(methodName, argTypes);
                    method.setAccessible(true);
                }
                catch (Exception e) {
                    failed = e;
                }
            }
            if(method == null) throw new UnableToFindMethodException(methodNames, failed);

            cachedMethods.put(Pair.of(clazz, getMethodSignature(methodNames[0], argTypes)), method);
            return method;
        }
        
        // This is used for the method cache and shouldn't be used to get a real method signature.
        // The clazz.getName() gets cached by java so it's still faster then doing the reflection
        private static String getMethodSignature(String methodName, Class<?>... argTypes) {
            String signature = methodName + "(";
            
            for(Class<?> clazz : argTypes) signature += clazz.getName();
            
            return signature + ")";
        }
    }
    
    public static class Constructors {
        private static Map<Pair<Class<?>, String>, Constructor<?>> cachedConstructors = new HashMap<Pair<Class<?>, String>, Constructor<?>>();

        public static <E> E invokeConstructor(String clazzName, Object... args) {
            try {
                return (E) invokeConstructor(Class.forName(clazzName), args);
            }
            catch(ClassNotFoundException e) {
                throw new ReflectionHelper.UnableToFindClassException(new String[] {clazzName}, e);
            }
        }
        
        public static <E> E invokeConstructor(Class<E> clazz, Object... args) {
            return invokeConstructor(clazz, args, ReflectionUtils.getClassArr(args));
        }

        public static <E> E invokeConstructor(Class<E> clazz, Object[] args, Class<?>... argTypes) {
            Constructor<E> constructor = findConstructor(clazz, argTypes);
            try {
                return constructor.newInstance(args);
            }
            catch(IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException e) {
                throw new Errors.UnableToInitiateConstructorException(e);
            }
        }
        
        public static <E> Constructor<E> findConstructor(Class<E> clazz, Class<?>... argTypes) {
            Constructor<E> constructor = (Constructor<E>) cachedConstructors.get(Pair.of(clazz, getConstructorSignature(argTypes)));
            
            if(constructor != null) return constructor;
            
            try {
                constructor = clazz.getDeclaredConstructor(argTypes);
                constructor.setAccessible(true);
            }
            catch (Exception e) {
                throw new Errors.UnableToFindConstructorException(e);
            }

            cachedConstructors.put(Pair.of(clazz, getConstructorSignature(argTypes)), constructor);
            return constructor;
        }
        
        // See method signature method
        private static String getConstructorSignature(Class<?>... argTypes) {
            String signature = "";
            
            for(Class<?> clazz : argTypes) signature += clazz.getName();
            
            return signature;
        }
    }
    
    public static class Errors {
        public static class UnableToInvokeMethodException extends RuntimeException {
            private static final long serialVersionUID = 1L;
            @SuppressWarnings("unused")
            private String[] methodNameList;

            public UnableToInvokeMethodException(String[] fieldNames, Exception e) {
                super(e);
                this.methodNameList = fieldNames;
            }
        }
        
        public static class UnableToFindConstructorException extends RuntimeException {
            private static final long serialVersionUID = 1L;

            public UnableToFindConstructorException(Exception failed) {
                super(failed);
            }
        }
        
        public static class UnableToInitiateConstructorException extends RuntimeException {
            private static final long serialVersionUID = 1L;

            public UnableToInitiateConstructorException(Exception e) {
                super(e);
            }
        }
    }
}
