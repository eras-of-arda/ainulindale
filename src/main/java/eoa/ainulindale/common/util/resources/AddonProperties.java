package eoa.ainulindale.common.util.resources;

import eoa.ainulindale.api.registries.MapRegistry;
import net.minecraft.util.ResourceLocation;

public class AddonProperties {
    public ResourceLocation mapID;
    public MapRegistry.MapInfo mapInfo;
}
