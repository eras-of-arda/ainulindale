package eoa.ainulindale.common.util.resources;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.imageio.ImageIO;
import org.apache.commons.io.IOUtils;
import com.google.common.base.Charsets;
import eoa.ainulindale.common.Ainulindale;

public class InputStreamUtils {
    public static BufferedReader getReader(InputStream in) {
        return new BufferedReader(new InputStreamReader(in, Charsets.UTF_8));
    }
    
    public static Map<String, BufferedReader> getInputStreams(Map<String, InputStream> ins) {
        Map<String, BufferedReader> map = new HashMap<String, BufferedReader>();
        
        for(Entry<String, InputStream> entry : ins.entrySet()) {
            map.put(entry.getKey(), getReader(entry.getValue()));
        }
        
        return map;
    }
    
    public static BufferedImage getImage(InputStream in) {
        try {
            return ImageIO.read(in);
        }
        catch(IOException e) {
            Ainulindale.log.error("Failed to convert a input stream into a buffered image.");
            e.printStackTrace();
        }
        finally {
            IOUtils.closeQuietly(in);
        }
        
        return null;
    }
}
