package eoa.ainulindale.common.util.resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModContainer;
import eoa.ainulindale.common.Ainulindale;
import net.minecraft.util.ResourceLocation;

public class ResourceHelper {
    public static InputStream getInputStream(ResourceLocation res) {
        return getInputStream(getContainer(res), getPath(res));
    }
    
    private static InputStream getInputStream(ModContainer container, String path) {
        return container.getClass().getResourceAsStream(path);
    }
    
    public static Map<String, InputStream> getInputStreams(ResourceLocation res, String... extensions) {
        Map<String, InputStream> map = new HashMap<String, InputStream>();
        
        for(Entry<String, ResourceLocation> entry : getSubFileResourceLocations(res, extensions).entrySet()) {
            map.put(entry.getKey(), getInputStream(entry.getValue()));
        }

        return map;
    }
    
    public static Map<String, ResourceLocation> getSubFileResourceLocations(ResourceLocation res, String... extensions) {
        Map<String, ResourceLocation> map = new HashMap<String, ResourceLocation>();
        String basePath = getPath(res);
        
        for(String file : getAllSubFilePaths(res, extensions)) {
            String name = file.replace("\\", "/");
            int startIndex = name.contains(basePath) ? name.lastIndexOf(basePath) + basePath.length() + (basePath.endsWith("/") ? 0 : 1): 0;
            int endIndex = name.indexOf(".");
            
            String newResPath = res.getResourcePath() + (res.getResourcePath().endsWith("/") ? "" : "/") + name.substring(startIndex, name.length());
            ResourceLocation fileRes = new ResourceLocation(res.getResourceDomain(), newResPath); 
            name = name.substring(startIndex, endIndex);
            
            map.put(name, fileRes);
        }

        return map;
    }
    
    public static List<String> getAllSubFilePaths(ResourceLocation res, String... extensions) {
        List<String> list = new ArrayList<String>();
        String baseStringPath = getPath(res);
        ModContainer container = getContainer(res);
        
        if(baseStringPath.contains(".")) {
            Ainulindale.log.error("Tried searching input stream in a file: " + res);
            throw new IllegalArgumentException("Can't search files in a file.");
        }
        
        try {
            URI uri = container.getClass().getResource(baseStringPath).toURI();

            Path basePath;
            FileSystem fileSystem = null;
            if (uri.getScheme().equals("jar")) {
                fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
                basePath = fileSystem.getPath(baseStringPath);
            } 
            else {
                basePath = Paths.get(uri);
            }
            
            Stream<Path> allFilePaths = Files.walk(basePath);
            for(Iterator<Path> it = allFilePaths.iterator(); it.hasNext();){
                Path filePath = it.next();
                String stringFilePath = filePath.toString();
                
                if(stringFilePath.contains(".")) {
                    String extension = stringFilePath.substring(stringFilePath.indexOf(".") + 1, stringFilePath.length());
                    
                    if(extensions.length != 0) {
                        for(String allowedExtension : extensions) {
                            if(allowedExtension.equalsIgnoreCase(extension)) {
                                list.add(stringFilePath);
                                break;
                            }
                        }
                    }
                    else {
                        list.add(stringFilePath);
                    }
                }
            }
            allFilePaths.close();
            if(fileSystem != null) fileSystem.close();
        }
        catch(URISyntaxException | IOException e) {
            Ainulindale.log.error("Something went wrong when searching for all files in: " + res);
            e.printStackTrace();
        }
        
        return list;
    }
    
    private static String getPath(ResourceLocation res) {
        return "/assets/" + res.getResourceDomain() + "/" + res.getResourcePath();
    }
    
    private static ModContainer getContainer(ResourceLocation res) {
        ModContainer modContainer = Loader.instance().getIndexedModList().get(res.getResourceDomain());
        if(modContainer == null) throw new IllegalArgumentException("Can't find the mod container for the domain " + res.getResourceDomain());
        return modContainer;
    }
}

