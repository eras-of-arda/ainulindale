package eoa.ainulindale.common.world.biome;

import lotr.common.world.spawning.LOTREventSpawner;

public class AinulindaleBiomeProperties {
    protected float minHeight = 0.0f;
    protected float maxHeight = 0.0f;
    protected float temp = 0.0f;
    protected float rain = 0.0f;
    protected LOTREventSpawner.EventChance banditChance = LOTREventSpawner.EventChance.BANDIT_RARE;

    public AinulindaleBiomeProperties()
    {

    }

    public AinulindaleBiomeProperties setMinMaxHeight(float min, float max)
    {
        this.minHeight = min;
        this.maxHeight = max;
        return this;
    }

    public AinulindaleBiomeProperties setTemperatureRainfall(float temp, float rain)
    {
        this.temp = temp;
        this.rain = rain;
        return this;
    }

    public AinulindaleBiomeProperties setBanditChance(LOTREventSpawner.EventChance chance)
    {
        this.banditChance = chance;
        return this;
    }
}
