package eoa.ainulindale.common.world.biome;

import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRBiomeGenMeneltarma;

public class BiomeGenReplacementMeneltarma extends LOTRBiomeGenMeneltarma implements HiddenBiome {

    public BiomeGenReplacementMeneltarma(int i, boolean major) {
        super(i, major);
    }

    @Override
    public LOTRBiome getHiddenReplacementBiome() {
        try {
            return (LOTRBiome)((LOTRBiome.class).getField("ocean").get(null));
        } catch (IllegalAccessException e) {
            return null;
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    @Override
    public LOTRBiome getRealBiome() {
        return this;
    }

}
