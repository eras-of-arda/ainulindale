package eoa.ainulindale.common.world.biome;

import lotr.common.world.biome.LOTRBiome;

// Don't know if I want to keep this or if I want to make it part of BaseLOTRBiome
public interface HiddenBiome {
    
    public LOTRBiome getHiddenReplacementBiome();
    
    public LOTRBiome getRealBiome();

}
