package eoa.ainulindale.core;

import java.util.Map;
import org.apache.logging.log4j.LogManager;
import cpw.mods.fml.relauncher.IFMLCallHook;
import eoa.ainulindale.core.patches.FMLPatcher;
import eoa.ainulindale.core.patches.LOTRPatcher;

public class AinulindaleCoreSetup implements IFMLCallHook {

    @Override
    public Void call() throws Exception {
        AinulindaleCoreMod.log = LogManager.getLogger("Ainulindalė Core");

        AinulindaleCoreMod.registerPatcher(new FMLPatcher());
        AinulindaleCoreMod.registerPatcher(new LOTRPatcher());

        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {
    }
}
