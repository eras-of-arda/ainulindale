package eoa.ainulindale.core.hooks;

import java.util.ArrayList;
import java.util.List;
import eoa.ainulindale.api.AinulindaleAPI;
import eoa.ainulindale.api.event.LOTRAchievementEarnedEvent;
import eoa.ainulindale.api.event.registry.WaypointRegistryEvent;
import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.client.HiddenBiomeCacheClient;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.network.AinulindaleNetworkHandler;
import eoa.ainulindale.common.network.S2C.PacketReloadHiddenBiomes;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import eoa.ainulindale.common.world.biome.HiddenBiome;
import lotr.client.LOTRTextures;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiome;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

public class EoACoreHooks {

    public static class Map {
        public static boolean firstClientReload = true;
        
        public static void loadActiveMap() {
            Ainulindale.log.info("ADDONLOAD 1");
            //AddonRegistry.loadAddon(MapRegistry.getActiveMap().getID());
            MapRegistry.loadMap(MapRegistry.getActiveMap().getID());
        }
        
        public static void reloadClientMap() {
            if(firstClientReload) {
                LOTRReflectionHelpers.LOTRMap.setClientSideMapTexture(LOTRTextures.missingTexture, LOTRTextures.missingTexture);
                firstClientReload = false;
            }
            
            MapRegistry.reloadActiveMap();
        }
        
        public static void sendClientMapReloadPacket(EntityPlayerMP player) {
            AinulindaleNetworkHandler.networkWrapper.sendTo(new PacketReloadHiddenBiomes(), player);
        }
        
        public static void earnedDisplayAchievement(LOTRAchievement achievement) {
            if(achievement.isBiomeAchievement) HiddenBiomeCacheClient.earnedBiomeAchievement(achievement);
        }
        
        public static LOTRBiome getHiddenBiomeReplacement(LOTRBiome biome) {
            // It may seems like a weird cast but it's safe, there is a instanceof check in the core modding
            HiddenBiome hiddenBiome = (HiddenBiome) biome;
            return HiddenBiomeCacheClient.currentHiddenBiomes.contains(hiddenBiome) ? hiddenBiome.getHiddenReplacementBiome() : hiddenBiome.getRealBiome();
        }
        
        public static LOTRMapLabels[] filterMapLabels(LOTRMapLabels[] labels) {
            List<LOTRMapLabels> list = new ArrayList<LOTRMapLabels>();
            
            for(LOTRMapLabels label : labels) {
                LOTRBiome biome = LOTRReflectionHelpers.LOTRMap.getLabelBiome(label);
                if(biome instanceof HiddenBiome && HiddenBiomeCacheClient.currentHiddenBiomes.contains(biome)) continue;
                
                list.add(label);
            }
            
            return list.toArray(new LOTRMapLabels[0]);
        }
    }
    
    public static class Events {
        public static void callWaypointRegistry() {
            AinulindaleAPI.LOTR_EVENT_BUS.post(new WaypointRegistryEvent());
        }

        public static void onAchievementEarned(EntityPlayer player, LOTRAchievement achievement) {
            AinulindaleAPI.LOTR_EVENT_BUS.post(new LOTRAchievementEarnedEvent(player, achievement));
        }
    }
    
}
