package eoa.ainulindale.core.patches;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import eoa.ainulindale.core.AinulindaleCoreMod;
import eoa.ainulindale.core.patches.base.Patcher;
import eoa.ainulindale.core.utils.ASMUtils;

public class FMLPatcher extends Patcher {

    public FMLPatcher() {
        super("FML");

        this.classes.put("cpw.mods.fml.common.LoadController", (classNode) -> patchLoaderController(classNode));
    }

    private void patchLoaderController(ClassNode classNode) {
        MethodNode method = ASMUtils.findMethod(classNode, "buildModList", "(Lcpw/mods/fml/common/event/FMLLoadEvent;)V");
        if(method == null) return;

        method.instructions.insert(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/PreMCHooks", "postFMLLoad", "()V", false));

        AinulindaleCoreMod.log.info("Patched the FML load controler.");
    }
}
