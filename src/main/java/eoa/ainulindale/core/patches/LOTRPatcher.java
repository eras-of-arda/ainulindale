package eoa.ainulindale.core.patches;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import eoa.ainulindale.core.AinulindaleCoreMod;
import eoa.ainulindale.core.patches.base.ModPatcher;
import eoa.ainulindale.core.utils.ASMUtils;

public class LOTRPatcher extends ModPatcher {

    public LOTRPatcher() {
        super("LOTR", "lotr");
        
        //map stuff
        this.classes.put("lotr.common.world.genlayer.LOTRGenLayerWorld", (classNode) -> hookMapLoad(classNode));
        this.classes.put("lotr.client.LOTRTextures", (classNode) -> hookClientMapLoad(classNode));
        
        //hidden biomes
        this.classes.put("lotr.common.network.LOTRPacketAchievement$Handler", (classNode) -> hookAchievementPacket(classNode));
        this.classes.put("lotr.client.gui.LOTRGuiMap", (classNode) -> hookMapGUI(classNode));
        
        
        //general lotr events
        this.classes.put("lotr.common.LOTRPlayerData", (classNode) -> patchPlayerData(classNode));

        //registry events
        this.classes.put("lotr.common.world.map.LOTRWaypoint", (classNode) -> hookWaypointLoad(classNode));
    }

    private void hookMapLoad(ClassNode classNode) {
        MethodNode method = ASMUtils.findMethod(classNode, "<init>", "()V");
        if(method == null) return;

        for(AbstractInsnNode node : method.instructions.toArray()) {
            if(node.getOpcode() == Opcodes.IFNE) {
                JumpInsnNode jumpNode = (JumpInsnNode) node;
                
                InsnList list = new InsnList();
                list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Map", "loadActiveMap", "()V", false));
                list.add(new InsnNode(Opcodes.RETURN));
                
                method.instructions.insert(jumpNode, list);
                break;
            }
        }

        AinulindaleCoreMod.log.info("Hooked into the initial LOTR Map loading.");
    }

    private void hookClientMapLoad(ClassNode classNode) {
        MethodNode method = ASMUtils.findMethod(classNode, "loadMapTextures", "()V");
        if(method == null) return;
        
        method.instructions.clear();
        method.localVariables.clear();
        method.tryCatchBlocks.clear();

        InsnList list = new InsnList();
        list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Map", "reloadClientMap", "()V", false));
        list.add(new InsnNode(Opcodes.RETURN));
        
        method.instructions.insert(list);
        
        AinulindaleCoreMod.log.info("Hooked into the Client LOTR Map loading.");
        
        
        MethodNode methodDrawMap = ASMUtils.findMethod(classNode, "drawMap", "(Lnet/minecraft/entity/player/EntityPlayer;ZDDDDDDDDDF)V");
        if(methodDrawMap == null) return;
        
        // Patch Meneltarma cover
        for(AbstractInsnNode node : methodDrawMap.instructions.toArray()) {
            if(node.getOpcode() == Opcodes.INVOKEVIRTUAL) {
                MethodInsnNode methodNode = (MethodInsnNode) node;
                
                if(methodNode.name.equals("draw") && methodNode.desc.equals("()I") && methodNode.owner.equals("net/minecraft/client/renderer/Tessellator")) {
                    InsnNode newReturnNode = new InsnNode(Opcodes.RETURN);
                    
                    methodDrawMap.instructions.insert(methodNode.getNext(), newReturnNode);
                    AinulindaleCoreMod.log.info("Patched LOTRTextures to disable the normal Meneltarma hiding.");
                    break;
                }   
            }
        }
    }
    
    private void hookAchievementPacket(ClassNode classNode) {
        MethodNode method = ASMUtils.findMethod(classNode, "onMessage", "(Llotr/common/network/LOTRPacketAchievement;Lcpw/mods/fml/common/network/simpleimpl/MessageContext;)Lcpw/mods/fml/common/network/simpleimpl/IMessage;");
        if(method == null) return;
        
        // Hook into achievements
        for(AbstractInsnNode node : method.instructions.toArray()) {
            if(node.getOpcode() == Opcodes.INVOKEVIRTUAL) {
                MethodInsnNode methodNode = (MethodInsnNode) node;
                
                if(methodNode.name.equals("queueAchievement") && methodNode.desc.equals("(Llotr/common/LOTRAchievement;)V") && methodNode.owner.equals("lotr/common/LOTRCommonProxy")) {
                    InsnList list = new InsnList();
                    list.add(new VarInsnNode(Opcodes.ALOAD, 3)); //load achievement
                    list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Map", "earnedDisplayAchievement", "(Llotr/common/LOTRAchievement;)V", false));
                    
                    method.instructions.insert(methodNode, list);
                    AinulindaleCoreMod.log.info("Hooked LOTRPacketAchievement for displayed achievements.");
                    break;
                }
                
            }
        }
    }
    
    private void hookMapGUI(ClassNode classNode) {
        MethodNode methodDrawScreen = ASMUtils.findMethod(classNode, "drawScreen", "func_73863_a", "(IIF)V");
        if(methodDrawScreen == null) return;
        
        // Hook into draw map
        for(AbstractInsnNode node : methodDrawScreen.instructions.toArray()) {
            if(node.getOpcode() == Opcodes.INVOKESTATIC) {
                MethodInsnNode methodNode = (MethodInsnNode) node;
                
                if(methodNode.name.equals("getBiomeOrOcean") && methodNode.desc.equals("(II)Llotr/common/world/biome/LOTRBiome;") && methodNode.owner.equals("lotr/common/world/genlayer/LOTRGenLayerWorld")) {
                    int biomeVarIndex = ((VarInsnNode) methodNode.getNext()).var;
                    
                    
                    LabelNode endIfLabelNode = new LabelNode();
                    
                    InsnList list = new InsnList();
                    list.add(new VarInsnNode(Opcodes.ALOAD, biomeVarIndex)); //load biome
                    list.add(new TypeInsnNode(Opcodes.INSTANCEOF, "eoa/ainulindale/common/world/biome/HiddenBiome")); //if true put 1 on stack
                    list.add(new JumpInsnNode(Opcodes.IFEQ, endIfLabelNode)); //if equal 0 goto label
                    list.add(new VarInsnNode(Opcodes.ALOAD, biomeVarIndex)); //load biome
                    list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Map", "getHiddenBiomeReplacement", "(Llotr/common/world/biome/LOTRBiome;)Llotr/common/world/biome/LOTRBiome;", false));
                    list.add(new VarInsnNode(Opcodes.ASTORE, biomeVarIndex)); //store returned biome
                    list.add(endIfLabelNode);
                    
                    AbstractInsnNode afterNode = methodNode.getNext();
                    methodDrawScreen.instructions.insert(afterNode, list);
                    AinulindaleCoreMod.log.info("Patched LOTRGuiMap to hide tooltips of hidden biomes.");
                    break;
                }
                
            }
        }
        
        
        MethodNode methodRenderLabel = ASMUtils.findMethod(classNode, "renderLabels", "()V");
        if(methodRenderLabel == null) return;
        
        // Hook into label render
        for(AbstractInsnNode node : methodRenderLabel.instructions.toArray()) {
            if(node.getOpcode() == Opcodes.INVOKESTATIC) {
                MethodInsnNode methodNode = (MethodInsnNode) node;
                
                if(methodNode.name.equals("allMapLabels") && methodNode.desc.equals("()[Llotr/client/gui/LOTRMapLabels;") && methodNode.owner.equals("lotr/client/gui/LOTRMapLabels")) {
                     MethodInsnNode newMethodNode = new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Map", "filterMapLabels", "([Llotr/client/gui/LOTRMapLabels;)[Llotr/client/gui/LOTRMapLabels;", false);
                    
                    methodRenderLabel.instructions.insert(methodNode, newMethodNode);
                    AinulindaleCoreMod.log.info("Patched LOTRGuiMap to hide labels of hidden biomes.");
                    break;
                }   
            }
        }
    }
    
    private void patchPlayerData(ClassNode classNode) {
        MethodNode methodAchievement = ASMUtils.findMethod(classNode, "addAchievement", "(Llotr/common/LOTRAchievement;)V");
        MethodNode methodSendPD = ASMUtils.findMethod(classNode, "sendPlayerData", "(Lnet/minecraft/entity/player/EntityPlayerMP;)V");
        if(methodAchievement == null) return;
        
        
        // Hook into achievements
        for(AbstractInsnNode node : methodAchievement.instructions.toArray()) {
            if(node.getOpcode() == Opcodes.INVOKESPECIAL) {
                MethodInsnNode methodNode = (MethodInsnNode) node;
                
                if(methodNode.name.equals("getPlayer") && methodNode.desc.equals("()Lnet/minecraft/entity/player/EntityPlayer;") && methodNode.owner.equals("lotr/common/LOTRPlayerData")) {
                    InsnList list = new InsnList();
                    list.add(new VarInsnNode(Opcodes.ALOAD, 2)); //load player
                    list.add(new VarInsnNode(Opcodes.ALOAD, 1)); //load achivement
                    list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Events", "onAchievementEarned", "(Lnet/minecraft/entity/player/EntityPlayer;Llotr/common/LOTRAchievement;)V", false));
                    
                    methodAchievement.instructions.insert(methodNode.getNext(), list);
                    AinulindaleCoreMod.log.info("Hooked LOTRPlayerData for the achievement event.");
                    break;
                }
                
            }
        }
        
        
        // Hook into send player data join
        InsnList list = new InsnList();
        list.add(new VarInsnNode(Opcodes.ALOAD, 1)); //load player
        list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Map", "sendClientMapReloadPacket", "(Lnet/minecraft/entity/player/EntityPlayerMP;)V", false));
                    
        methodAchievement.instructions.insertBefore(methodSendPD.instructions.getLast().getPrevious(), list);
        AinulindaleCoreMod.log.info("Hooked LOTRPlayerData for the server login map reload.");
    }

    private void hookWaypointLoad(ClassNode classNode) {
        MethodNode method = ASMUtils.findMethod(classNode, "<clinit>", "()V");
        if(method == null) return;
        
        AbstractInsnNode lastNode = method.instructions.getLast();
        
        if(lastNode.getOpcode() == Opcodes.RETURN) {
            method.instructions.insertBefore(lastNode, new MethodInsnNode(Opcodes.INVOKESTATIC, "eoa/ainulindale/core/hooks/EoACoreHooks$Events", "callWaypointRegistry", "()V", false));

            AinulindaleCoreMod.log.info("Hooked LOTR Waypoint loading.");
        }
    }
}
