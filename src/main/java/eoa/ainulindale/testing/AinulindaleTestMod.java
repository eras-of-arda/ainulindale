package eoa.ainulindale.testing;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.api.Submod;
import eoa.ainulindale.api.SubmodMarker;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.api.registries.MapRegistry.OverlayMapInfo;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.CommonProxy;
import eoa.ainulindale.common.reflection.ReflectionUtils;
import net.minecraft.util.ResourceLocation;

@Mod(modid = AinulindaleTestMod.MODID, version = AinulindaleTestMod.VERSION, name = AinulindaleTestMod.NAME, dependencies = "required-after:ainulindale", acceptableRemoteVersions = "*")
@SubmodMarker(submodid = AinulindaleTestMod.MODID)
public class AinulindaleTestMod extends Submod{
    public static final String MODID = "ainulindaletestmod";
    public static final String NAME = "Ainulindalė";
    public static final String VERSION = "0.0.1";
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) throws InterruptedException {
//        try {
//            System.out.println("Start");
//            initReflect();
//            initReflect();
//            initReflect();
//            testReflection();
//            testReflection();
//            System.out.println("End");
//        }
//        catch(Throwable e) {
//            e.printStackTrace();
//        }
    }
    
    @Override
    public void loadSubmod() {
        MapRegistry.registerMap(new OverlayMapInfo(new ResourceLocation(Ainulindale.MODID, "map.png"), new ResourceLocation(Ainulindale.MODID, "overlay.png")));
    }
    
    @Override
    public void enableSubmod() {
        MapRegistry.setActiveMap(new ResourceLocation(Ainulindale.MODID, "map.png"));
    }
    
    @Override
    public void disableSubmod() {
        MapRegistry.setActiveMap(MapRegistry.defaultMap);
    }
    
    @SuppressWarnings("unused")
    public static void testReflection() throws Throwable {
        System.out.println("Start batch");
        int loopCyles = 100000;
        
        long startTimeNew = System.currentTimeMillis();
        for(int i = 0; i < loopCyles; i++) ReflectionUtils.Fields.getPrivateValue(Ainulindale.class, null, "proxy");
        System.out.println("Took " + (System.currentTimeMillis() - startTimeNew) + " ms for new method.");

        long startTimeBase = System.currentTimeMillis();
        for(int i = 0; i < loopCyles; i++) {
            Field f = Ainulindale.class.getDeclaredField("proxy");
            f.setAccessible(true);
            f.get(null);
        }
        System.out.println("Took " + (System.currentTimeMillis() - startTimeBase) + " ms for base method.");
        
        long startTimeBaseIpv = System.currentTimeMillis();
        Field f =  Ainulindale.class.getDeclaredField("proxy");
        f.setAccessible(true);
        for(int i = 0; i < loopCyles; i++) {
            f.get(null);
        }
        System.out.println("Took " + (System.currentTimeMillis() - startTimeBaseIpv) + " ms for base ipv method.");
        
        long startTimeOld = System.currentTimeMillis();
        for(int i = 0; i < loopCyles; i++) ReflectionHelper.getPrivateValue(Ainulindale.class, null, "proxy");
        System.out.println("Took " + (System.currentTimeMillis() - startTimeOld) + " ms for old method.");

        MethodHandle handle = MethodHandles.lookup().unreflectGetter(f);
        long startTimeHandles = System.currentTimeMillis();
        for(int i = 0; i < loopCyles; i++) {
            CommonProxy proxy = (CommonProxy) handle.invokeExact();
        }
        System.out.println("Took " + (System.currentTimeMillis() - startTimeHandles) + " ms for handle method.");

        System.out.println("End batch");
    }
    
    public static void initReflect() {
        long startTimeNew = System.currentTimeMillis();
        ReflectionUtils.Fields.getPrivateValue(Ainulindale.class, null, "proxy");
        System.out.println("Took " + (System.currentTimeMillis() - startTimeNew) + " ms for init.");
    }
}
